from pathlib import Path

js_vendor_path = Path('assets/vendor/js')
css_vendor_path = Path('assets/vendor/css')

js_min_path = Path('static/js/vendor/main.min.js')
css_min_path = Path('static/css/vendor/main.min.css')

js_min_path.parent.mkdir(parents=True, exist_ok=True)
css_min_path.parent.mkdir(parents=True, exist_ok=True)


def get_file(path: Path) -> str:
    with open(path, 'r') as f_in:
        return f_in.read() + '\n\n'


js_files = [js_vendor_path / f
            for f in [
                # 'jquery.min.js',
                # 'jquery.mark.min.js',
                'leaflet.min.js',
                # 'medium-zoom.esm.js',
                # 'bootstrap.bundle.min.js',
                'fuse.min.js',
                # 'headroom.min.js',
                # 'instantpage.js',
                'isotope.min.js'
            ]]

with open(js_min_path, 'w') as f_js:
    for f in js_files:
        js = get_file(f)
        f_js.write(js)

with open(css_min_path, 'w') as f_css:
    for f in css_vendor_path.glob('*.css'):
        f_css.write(get_file(f))
