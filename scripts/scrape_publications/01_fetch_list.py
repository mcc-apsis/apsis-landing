import re
import json
import requests
from bs4 import BeautifulSoup

with open('publications_01.jsonl', 'w') as f_out:
    for page_i in range(1, 13):
        url = 'https://www.mcc-berlin.net/forschung/publikationen.html' \
              f'?tx_news_pi1%5B%40widget_0%5D%5BcurrentPage%5D={page_i}' \
              '&tx_news_pi1%5Bsearch%5D%5Bcategory%5D%5B2%5D=72' \
              '&tx_news_pi1%5B__referrer%5D%5B%40action%5D=extendedSearch' \
              '&tx_news_pi1%5B__trustedProperties%5D=a%3A1%3A%7Bs%3A6%3A%22search%22%3Ba%3A1%3A%7Bs%3A7%3A%22subject%22%3Bi%3A1%3B%7D%7Dd118e659a03a125bd8cbdea7342b9bb2bf05d9ae'
        print(f'Fetching page {page_i}')
        print(url)
        page = requests.get(url)
        print('  - received')
        soup = BeautifulSoup(page.text, features='lxml')
        print('  - parsed')

        publications = soup.css.select('div.pub')
        print(f'  - found {len(publications)} publications on this page')
        for pi, pub in enumerate(publications):
            print(f'    -> processing publication {pi + 1}/{len(publications)}')
            title = pub.css.select('h2.pub_title')[0]

            authors = pub.css.select('span.cat_author1')[0].get_text()
            authors = authors.replace('\n', '').strip().strip(',')
            authors = re.split(r',|;', authors)
            authors = [(authors[(ai * 2) + 1].strip(), authors[ai * 2].strip()) for ai in range(len(authors) // 2)]

            pub_obj = {
                'title': title.get_text().strip(),
                'authors': authors,
                'year': int(pub.css.select('span.pubdate')[0].get_text()),
                'url': 'https://mcc-berlin.net/' + title.css.select('a')[0]['href'],
                'venue': pub.css.select(':nth-child(3)')[0].get_text().strip()
            }
            print(f'    -> {pub_obj}')
            f_out.write(json.dumps(pub_obj) + '\n')
