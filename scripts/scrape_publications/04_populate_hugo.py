import re
import json
from pathlib import Path
from typing import Any

base_path = Path('../../content/publication').resolve()


def slug(s):
    return '-'.join([
        t
        for t in re.sub(r'\s+', ' ', re.sub(r'[^a-z0-9 ]', ' ', s.lower())).split(' ')
        if t not in ['the', 'for', 'a', 'in', 'on']
    ])


# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
types = {  # S2 publicationType to hugo category
    'Uncategorized': '0',
    'Review': '2',
    'JournalArticle': '2',
    'Editorial': '0',
    'News': '4'
}

author_overrides = {
    'minx': 'Jan Minx',
    'lamb': 'William Lamb',
    'repke': 'Tim Repke',
    'hansen': 'Finn Müller-Hansen',
    'callaghan': 'Max Callaghan',
    'döbbeling': 'Niklas Döbbeling',
    'doebbeling': 'Niklas Döbbeling',
    'dobbeling': 'Niklas Döbbeling',
    'dbbeling': 'Niklas Döbbeling',
    'lück': 'Sarah Lück',
    'lueck': 'Sarah Lück',
    'luck': 'Sarah Lück',
    'lck': 'Sarah Lück',
    'miersch': 'Klaas Miersch'
}


def get_override(s):
    for trigger in author_overrides.keys():
        if trigger in s.lower():
            return author_overrides[trigger]
    return s


def safe_get(obj, chain: list[tuple[str, Any]]):
    curr = obj
    for key, alt in chain:
        if type(curr) == dict and key in curr:
            if curr[key] is not None:
                curr = curr[key]
            else:
                curr = alt
        else:
            curr = alt
    return curr


with open('publications_03.jsonl', 'r') as f_in:
    for li, line in enumerate(f_in):
        print(f'Processing publication {li}')
        pub = json.loads(line)
        t_slug = slug(pub['title'])

        target_path = base_path / str(pub['year']) / t_slug
        target_path.mkdir(parents=True, exist_ok=True)
        print(f'  - writing to {target_path}')

        # prepare publication date
        pdate = None
        if pub.get('s2'):
            pdate = pub['s2'].get('publicationDate')
            if pdate is not None:
                pdate += 'T00:00:00Z'
        if pdate is None:
            pdate = f'{pub["year"]}-01-01T00:00:00Z'

        # prepare author list
        authors = []
        if pub.get('s2'):
            authors = [a['name'] for a in pub['s2']['authors']]

        if len(authors) == 0:
            authors = [f'{author[0]} {author[1]}' for author in pub['authors']]

        authors = [get_override(author) for author in authors]

        if len(authors) > 0:
            authors_str = '\n  - ' + ('\n  - '.join(authors))
        else:
            authors_str = []

        ptypes = pub.get('s2', {'publicationTypes': None})['publicationTypes']
        if ptypes is None or len(ptypes) == 0:
            ptypes = ['Uncategorized']
        ptypes_int = [types[pt] for pt in ptypes]

        abstract = pub.get('s2', pub).get('abstract', '')
        if abstract is None:
            abstract = pub['abstract']

        doi = pub.get('s2', {}).get('externalIds', {}).get('DOI', '') or ''

        venue = pub.get('s2', {}).get('booktitle', pub['venue'])
        if venue is None:
            venue = ''
        venue = venue.replace('"', "''")

        page = f"""
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "{pub['title']}"
authors: {authors_str}
date: {pdate}
doi: "{doi}"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-00-01T13:37:42+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: {json.dumps(ptypes_int)}

# Publication name and optional abbreviated publication name.
publication: "{venue}"
publication_short: ""

abstract: "{abstract}"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: {safe_get(pub, [('s2', {}), ('openAccessPdf', {}), ('url', None)]) or ''}
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        """

        with open(target_path / 'index.md', 'w') as f_index:
            f_index.write(page)

        bid = f'{pub["year"]}-'
        bid += pub['authors'][0][1] if len(pub['authors']) > 0 else 'anonymous'
        bid += '-' + t_slug.split('-')[0]

        bibtex = f"""
@article{{{bid},
  title={{{pub["title"]}}},
  author={{{' and '.join(authors)}}},
  journal={{{venue}}},
  pages={{1--6}},
  year={{{pub['year']}}},
  doi={{{doi}}}
}}
        """
        with open(target_path / 'cite.bib', 'w') as f_bib:
            f_bib.write(bibtex)
