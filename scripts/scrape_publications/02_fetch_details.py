import json
import time
from multiprocessing import Pool

import requests
from bs4 import BeautifulSoup


def process_pub(data):
    try:
        pi, pub = data
        t0 = time.time()
        url = pub['url'] \
            .replace('//forschung/publikationen/publikationen-detail',
                     '/en/research/publications/publications-detail') \
            .replace('https://',
                     'https://www.')
        print(f'[{pi: >3}] Requesting {url}')
        page = requests.get(url)
        print(f'[{pi: >3}] Received after {time.time() - t0:.0f}s.')

        soup = BeautifulSoup(page.text, features='lxml')
        print(f'[{pi: >3}] Parsed.')

        links = soup.css.select('.article .news-related-links ul>li>a')
        pub['links'] = [link['href'] for link in links]

        abstract = soup.css.select('.article div.news-single-item p:nth-of-type(3)')
        if len(abstract) > 0:
            pub['abstract'] = abstract[0].get_text()
        else:
            pub['abstract'] = None

        return pub
    except Exception as e:
        print(f'Error {e} for {pi}')
        raise e


if __name__ == '__main__':
    with open('publications_01.jsonl', 'r') as f_in:
        publications = [(li, json.loads(line)) for li, line in enumerate(f_in)]

    print('Spinning up processing pool...')
    with Pool(8) as p:
        publications_ext = p.map(process_pub, publications)

    print('All done, writing results...')
    with open('publications_02.jsonl', 'w') as f_out:
        [f_out.write(json.dumps(pe) + '\n') for pe in publications_ext]

    print('Finished!')
