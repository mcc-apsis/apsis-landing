---
block: markdown
headless: true # This file represents a page section.
weight: 10 # Order that this section will appear.
title: |
  Evidence for  
  Climate   
  Solutions
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: '2'
  # Add custom styles
  css_style:
  css_class:
---

The [ECS working group](https://www.mcc-berlin.net/en/research/working-groups/applied-sustainability-science.html) *(Evidence for climate solutions; formerly known as APSIS: Applied sustainability science)* is interested in understanding how scientific information is compiled and made relevant for climate policy. 
In the face of the rapidly expanding volume of literature on climate change, 
the researchers aim to contribute to the development and application of methods for research synthesis. 
By doing so, they help transform individual pieces of research and information into a more coherent map of decision-relevant knowledge, 
thereby realigning the social sciences in climate change research towards a solution-oriented paradigm.
