---

block: markdown
headless: true # This file represents a page section.
weight: 20 # Order that this section will appear.
title: Quick Links
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: '1'
  # Add custom styles
  css_style:
  css_class:
---

## NACSOS 
* :star: [Scoping / The legacy platform](https://apsis.mcc-berlin.net/nacsos-legacy/)
* :star: [The new platform](https://apsis.mcc-berlin.net/nacsos/)
* [Documentation](https://apsis.mcc-berlin.net/nacsos-docs/)
* [NACSOS-core API](https://apsis.mcc-berlin.net/nacsos-core/docs)
* [NACSOS-pipelines API](https://apsis.mcc-berlin.net/nacsos-pipes/docs)

## Interactive tools
* [Climate & Health App](https://apsis.mcc-berlin.net/climate-health/)
* [Climate Policy Instruments Map](https://apsis.mcc-berlin.net/climate-policy-instruments-map)
* [Impact Map](https://mcallaghan.github.io/interactive-impacts-map/)
* [Literature Hub](https://climateliterature.org)

## MCC-internal
* [Table Booking](https://apsis.mcc-berlin.net/tbs)
* [Lunch Lottery](https://apsis.mcc-berlin.net/nacsos-legacy/lotto)
