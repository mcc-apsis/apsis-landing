---

block: markdown
headless: true # This file represents a page section.
weight: 5 # Order that this section will appear.
title: “Evidence for Climate Solutions” (ECS) Group Members
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: '1'
  # Add custom styles
  css_style:
  css_class:
---

{{< figure src="group.png" id="group" title="Group photo September 2023" img_class="ad" >}}
