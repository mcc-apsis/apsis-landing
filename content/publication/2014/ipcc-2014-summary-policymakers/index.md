
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "IPCC, 2014: Summary for Policymakers"
authors: 
  - Field
  - Barros
  - Mastrandrea
  - Mach
  - Abdrabo
  - N. Adger
  - Anokhin
  - Anisimov
  - Arent
  - Jonathon Barnett Australia
  - Burkett
  - R. China
  - Monalisa Chatterjee India
  - Stewart J. Cohen Canada
  - Purnamita Dasgupta India
  - Davidson
  - Fatima Denton Gambia
  - K. Dow
  - Ove Hoegh-Guldberg Australia
  - Jones
  - Kitching
  - S. Kovats
  - Joan Nymand Larsen Iceland
  - Erda Lin China
  - Lobell
  - Losada
  - Magrin
  - Marengo
  - Anil Markandya Spain
  - McCarl
  - Mclean
  - Mearns
  - Midgley
  - Morton
  - Isabelle Niang Senegal
  - Noble
  - Nurse
  - O'brien
  - Lennart Olsson Sweden
  - M. Oppenheimer
  - Overpeck
  - Pereira
  - Poloczanska
  - Porter
  - Prather
  - Pulwarty
  - Andy Reisinger Zealand
  - Aromar Revi
  - Patricia Romero-Lankao Mexico
  - Ruppel
  - Satterthwaite
  - Schmidt
  - Smith
  - Stone
  - S. Africa.
  - Dáithí A. Usa
  - Suárez
  - P. Tschakert
  - Alicia Villamizar Venezuela
  - Rachel Warren Uk
  - Wilbanks
  - Poh Poh Wong Singapore
  - Alistair Woodward Zealand
  - Yohe
date: 2014-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "None"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
