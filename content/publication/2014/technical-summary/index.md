
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Technical Summary"
authors: 
  - E. Hertwich
  - Thomas Gibon
  - Anders Arvesen
  - P. Bayer
  - E. Bouman
  - G. Heath
  - A. Ramirez
  - S. Suh
date: 2022-04-30T00:00:00Z
doi: "10.18356/30fc26a1-en"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "Introduction Cell-cell fusion is a key event in fertilization and during differentiation of muscle, bone and trophoblast cells, and possibly also in stem-cell plasticity, carcinogenesis and tumor progression (Chen et al. Earlier work on fusion mechanisms has been focused on the earliest stages of the fusion pathways, which bring about the first measurable indications of fusion: lipid mixing and the opening of a narrow fusion pore – an aqueous connection between the membranes (Earp et al. Weissenhorn et al., 2007). At later stages of cell-cell fusion, these initial pores of a few nanometers in diameter expand to pores that are readily detectable by fluorescence microscopy (diameter >~0.2 μm) and finally yield an open lumen of cell-size diameter (~10-15 μm). Little is known about the properties of these larger pores and the mechanisms that underlie the enlargement of cytoplasmic bridges from early fusion pores to syncytia (Gattegno et al. For instance, we still do not know whether this enlargement proceeds spontaneously and, if not, whether it is driven by the cytoskeleton (Podbilewicz and White, 1994; Zheng and Chang, 1991), membrane tension (Knutton, 1980) or another, as-yet unidentified, cell machinery. The role of the actin cytoskeleton in cell-cell fusion has drawn special attention in many recent studies. Syncytium formation,"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
