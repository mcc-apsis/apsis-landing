
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The technological and economic prospects for CO2 utilisation and removal"
authors: 
  - C. Hepburn
  - E. Adlen
  - J. Beddington
  - E. Carter
  - S. Fuss
  - N. Mac Dowell
  - Jan Minx
  - P. Smith
  - C. Williams
date: 2019-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature"
publication_short: ""

abstract: "The capture and use of carbon dioxide to create valuable products might lower the net costs of reducing emissions or removing carbon dioxide from the atmosphere. Here we review ten pathways for the utilization of carbon dioxide. Pathways that involve chemicals, fuels and microalgae might reduce emissions of carbon dioxide but have limited potential for its removal, whereas pathways that involve construction materials can both utilize and remove carbon dioxide. Land-based pathways can increase agricultural output and remove carbon dioxide. Our assessment suggests that each pathway could scale to over 0.5 gigatonnes of carbon dioxide utilization annually. However, barriers to implementation remain substantial and resource constraints prevent the simultaneous deployment of all pathways."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
