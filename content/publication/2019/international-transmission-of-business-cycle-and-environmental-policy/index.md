
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "International Transmission of the Business Cycle and Environmental Policy"
authors: 
  - Barbara Annicchiarico
  - Francesca Diluiso
date: 2017-12-18T00:00:00Z
doi: "10.2139/ssrn.3089914"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Resource and Energy Economics"
publication_short: ""

abstract: "Abstract This paper presents a dynamic stochastic general equilibrium (DSGE) model of environmental policy for a two-country economy and studies the international transmission of asymmetric shocks considering two different economy-wide greenhouse gases (GHG) emission regulations: a carbon tax and a cap-and-trade system allowing for cross-border exchange of emission permits. We find that international spillovers of shocks are strongly influenced by the environmental regime put in place. The cross-border reaction to shocks is found to be magnified under a carbon tax. The pattern of trade and the underlying monetary regime influence the international transmission channels interacting with the environmental policy adopted."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
