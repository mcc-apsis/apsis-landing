
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Implications of deep decarbonization pathways for sustainable development"
authors: 
  - S. Fuss
date: 2019-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "In: What Next for Sustainable Development? Our Common Future at Thirty, eds. G Meadowcroft, J., Banister, D., Holden, E., Langhelle, O., Linnerud, K., Gilpin, Edward Elgar Publishing: 76–95"
publication_short: ""

abstract: "This book examines the international experience with sustainable development since the concept was brought to world-wide attention in Our Common Future, the 1987 report of the World Commission on Environment and Development. Scholars from a variety of disciplinary backgrounds engage with three critical themes: negotiating environmental limits; equity, environment and development; and transitions and transformations. In light of the 2030 Sustainable Development Goals recently adopted by the United Nations General Assembly, they ask what lies ahead for sustainable development."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
