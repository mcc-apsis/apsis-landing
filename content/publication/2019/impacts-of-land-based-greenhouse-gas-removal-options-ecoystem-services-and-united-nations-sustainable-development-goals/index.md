
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Impacts of Land-Based Greenhouse Gas Removal Options on Ecoystem Services and the United Nations Sustainable Development Goals"
authors: 
  - Pete Smith
  - Justin Adams
  - D. Beerling
  - T. Beringer
  - K. Calvin
  - S. Fuss
  - B. Griscom
  - N. Hagemann
  - C. Kammann
  - F. Kraxner
  - Jan Minx
  - A. Popp
  - P. Renforth
  - J. L. V. Vicente
  - S. Keesstra
date: 2019-06-11T00:00:00Z
doi: "10.1146/annurev-environ-101718-033129"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Annual Review of Environment and Resources"
publication_short: ""

abstract: "Land-based greenhouse gas removal (GGR) options include afforestation or reforestation (AR), wetland restoration, soil carbon sequestration (SCS), biochar, terrestrial enhanced weathering (TEW), and bioenergy with carbon capture and storage (BECCS). We assess the opportunities and risks associated with these options through the lens of their potential impacts on ecosystems services (Nature's Contributions to People; NCPs) and the United Nations Sustainable Development Goals (SDGs). We find that all land-based GGR options contribute positively to at least some NCPs and SDGs. Wetland restoration and SCS almost exclusively deliver positive impacts. A few GGR options, such as afforestation, BECCS, and biochar potentially impact negatively some NCPs and SDGs, particularly when implemented at scale, largely through competition for land. For those that present risks or are least understood, more research is required, and demonstration projects need to proceed with caution. For options that present low risks and provide cobenefits, implementation can proceed more rapidly following no-regrets principles."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.annualreviews.org/doi/pdf/10.1146/annurev-environ-101718-033129
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
