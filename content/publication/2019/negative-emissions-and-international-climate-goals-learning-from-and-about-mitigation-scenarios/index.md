
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Negative emissions and international climate goals—learning from and about mitigation scenarios"
authors: 
  - Jérôme Hilaire
  - Jan Minx
  - Max Callaghan
  - J. Edmonds
  - G. Luderer
  - G. Nemet
  - J. Rogelj
  - María del Mar Zamora
date: 2019-10-17T00:00:00Z
doi: "10.1007/s10584-019-02516-4"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Climatic Change"
publication_short: ""

abstract: "For aiming to keep global warming well-below 2 °C and pursue efforts to limit it to 1.5 °C, as set out in the Paris Agreement, a full-fledged assessment of negative emission technologies (NETs) that remove carbon dioxide from the atmosphere is crucial to inform science-based policy making. With the Paris Agreement in mind, we re-analyse available scenario evidence to understand the roles of NETs in 1.5 °C and 2 °C scenarios and, for the first time, link this to a systematic review of findings in the underlying literature. In line with previous research, we find that keeping warming below 1.5 °C requires a rapid large-scale deployment of NETs, while for 2 °C, we can still limit NET deployment substantially by ratcheting up near-term mitigation ambition. Most recent evidence stresses the importance of future socio-economic conditions in determining the flexibility of NET deployment and suggests opportunities for hedging technology risks by adopting portfolios of NETs. Importantly, our thematic review highlights that there is a much richer set of findings on NETs than commonly reflected upon both in scientific assessments and available reviews. In particular, beyond the common findings on NETs underpinned by dozens of studies around early scale-up, the changing shape of net emission pathways or greater flexibility in the timing of climate policies, there is a suite of “niche and emerging findings”, e.g. around innovation needs and rapid technological change, termination of NETs at the end of the twenty-first century or the impacts of climate change on the effectiveness of NETs that have not been widely appreciated. Future research needs to explore the role of climate damages on NET uptake, better understand the geophysical constraints of NET deployment (e.g. water, geological storage, climate feedbacks), and provide a more systematic assessment of NET portfolios in the context of sustainable development goals."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://link.springer.com/content/pdf/10.1007/s10584-019-02516-4.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
