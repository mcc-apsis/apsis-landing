
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Learning about urban climate solutions from case studies"
authors: 
  - William Lamb
  - F. Creutzig
  - Max Callaghan
  - Jan Minx
date: 2019-04-01T00:00:00Z
doi: "10.1038/s41558-019-0440-x"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "Climate mitigation research puts increasing emphasis on cities, but much  more could be learned from urban case studies. The overall size,  geographic scope and topic content of cases remains unknown, resulting  in few attempts to synthesise the bottom-up evidence. Here, we use  scientometric and machine-learning methods to produce a comprehensive  map of the literature. Our database of approximately 4,000 case studies  provides a wealth of evidence to search, compare and review. We find  that cities in world regions with the highest future mitigation  relevance are systematically underrepresented. A map of the evidence  allows case studies to be matched with urban typologies in new and more  ambitious forms of synthesis, bringing together traditionally separate  strands of qualitative and quantitative urban research.n."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
