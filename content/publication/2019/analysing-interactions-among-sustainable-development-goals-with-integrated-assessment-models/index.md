
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Analysing interactions among Sustainable Development Goals with Integrated Assessment Models"
authors: 
  - H. V. Soest
  - D. P. Vuuren
  - Jérôme Hilaire
  - Jan Minx
  - M. Harmsen
  - V. Krey
  - A. Popp
  - K. Riahi
  - G. Luderer
date: 2019-01-01T00:00:00Z
doi: "10.1016/j.glt.2019.10.004"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Global Transitions"
publication_short: ""

abstract: "To achieve all Sustainable Development Goals (SDGs) by 2030, it is necessary to understand how they interact with each other. Integrated Assessment Models (IAMs) represent many human–environment interactions and can inform policymakers about the synergies and trade-offs involved in meeting multiple goals simultaneously. We analyse how IAMs, originally developed to study interactions among energy, the economy, climate, and land, can contribute to a wider analysis of the SDGs in order to inform integrated policies. We compare the key interactions identified among the SDGs in an expert survey, with their current and planned representation in models as identified in a survey among modellers. We also use text mining to reveal past practices by extracting the themes discussed in the IAM literature, linking them to the SDGs, and identifying the interactions among them, thus corroborating our previous results. This combination of methods allowed us to discuss the role of modelling in informing policy coherence and stimulate discussions on future research. The analysis shows that IAMs cover the SDGs related to climate because of their design. It also shows that most IAMs cover several other areas that are related to resource use and the Earth system as well. Some other dimensions of the 2030 Agenda are also covered, but socio-political and equality goals, and others related to human development and governance, are not well represented. Some of these are difficult to capture in models. Therefore, it is necessary to facilitate a better representation of heterogeneity (greater geographical and sectoral detail) by using different types of models (e.g. national and global) and linking different disciplines (especially social sciences) together. Planned developments include increased coverage of human development goals and contribute to policy coherence."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
