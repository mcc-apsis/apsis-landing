
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Assessing human and environmental pressures of global land-use change 2000–2010"
authors: 
  - F. Creutzig
  - C. Bren d’Amour
  - U. Weddige
  - S. Fuss
  - T. Beringer
  - Anne Gläser
  - M. Kalkuhl
  - J. Steckel
  - Alexander Radebach
  - O. Edenhofer
date: 2019-01-01T00:00:00Z
doi: "10.1017/sus.2018.15"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Mitigation and Adaptation Strategies for Global Change"
publication_short: ""

abstract: "Non-technical summary Global land is turning into an increasingly scarce resource. We here present a comprehensive assessment of co-occuring land-use change from 2000 until 2010, compiling existing spatially explicit data sources for different land uses, and building on a rich literature addressing specific land-use changes in all world regions. This review systematically categorizes patterns of land use, including regional urbanization and agricultural expansion but also globally telecoupled land-use change for all world regions. Managing land-use change patterns across the globe requires global governance. Technical summary Here we present a comprehensive assessment of the extent and density of multiple drivers and impacts of land-use change. We combine and reanalyze spatially explicit data of global land-use change between 2000 and 2010 for population, livestock, cropland, terrestrial carbon and biodiversity. We find pervasive pressure on biodiversity but varying patterns of gross land-use changes across world regions. Our findings enable a classification of land-use patterns into three types. The ‘consumers’ type, displayed in Europe and North America, features high land footprints, reduced direct human pressures due to intensification of agriculture, and increased reliance on imports, enabling a partial recovery of terrestrial carbon and reducing pressure on biodiversity. In the ‘producer’ type, most clearly epitomized by Latin America, telecoupled land-use links drive biodiversity and carbon loss. In the ‘mover’ type, we find strong direct domestic pressures, but with a wide variety of outcomes, ranging from a concurrent expansion of population, livestock and croplands in Sub-Saharan Africa at the cost of natural habitats to strong pressure on cropland by urbanization in Eastern Asia. In addition, anthropogenic climate change has already left a distinct footprint on global land-use change. Our data- and literature-based assessment reveals region-specific opportunities for managing global land-use change."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.cambridge.org/core/services/aop-cambridge-core/content/view/307BC019C33F9C0914C131F888D5EB2E/S2059479818000157a.pdf/div-class-title-assessing-human-and-environmental-pressures-of-global-land-use-change-2000-2010-div.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
