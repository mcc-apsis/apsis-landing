
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Upscaling urban data science for global climate solutions"
authors: 
  - F. Creutzig
  - S. Lohrey
  - X. Bai
  - A. Baklanov
  - R. Dawson
  - S. Dhakal
  - William Lamb
  - T. McPhearson
  - Jan Minx
  - Esteban Muñoz
  - Brenna Walsh
date: 2019-01-01T00:00:00Z
doi: "10.1017/sus.2018.16"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Global Sustainability"
publication_short: ""

abstract: "Non-technical summary Manhattan, Berlin and New Delhi all need to take action to adapt to climate change and to reduce greenhouse gas emissions. While case studies on these cities provide valuable insights, comparability and scalability remain sidelined. It is therefore timely to review the state-of-the-art in data infrastructures, including earth observations, social media data, and how they could be better integrated to advance climate change science in cities and urban areas. We present three routes for expanding knowledge on global urban areas: mainstreaming data collections, amplifying the use of big data and taking further advantage of computational methods to analyse qualitative data to gain new insights. These data-based approaches have the potential to upscale urban climate solutions and effect change at the global scale. Technical summary Cities have an increasingly integral role in addressing climate change. To gain a common understanding of solutions, we require adequate and representative data of urban areas, including data on related greenhouse gas emissions, climate threats and of socio-economic contexts. Here, we review the current state of urban data science in the context of climate change, investigating the contribution of urban metabolism studies, remote sensing, big data approaches, urban economics, urban climate and weather studies. We outline three routes for upscaling urban data science for global climate solutions: 1) Mainstreaming and harmonizing data collection in cities worldwide; 2) Exploiting big data and machine learning to scale solutions while maintaining privacy; 3) Applying computational techniques and data science methods to analyse published qualitative information for the systematization and understanding of first-order climate effects and solutions. Collaborative efforts towards a joint data platform and integrated urban services would provide the quantitative foundations of the emerging global urban sustainability science."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.cambridge.org/core/services/aop-cambridge-core/content/view/D2D622B43CD50A9B2FD5DF855BCC0F18/S2059479818000169a.pdf/div-class-title-upscaling-urban-data-science-for-global-climate-solutions-div.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
