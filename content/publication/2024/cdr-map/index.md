---
title: "Scientific literature on carbon dioxide removal much larger than previously suggested: insights from an AI-enhanced systematic map"
authors:
  - Sarah Lück
  - Max Callaghan
  - Malgorzata Borchers
  - Annette Cowie
  - Sabine Fuss
  - Oliver Geden
  - Matthew Gidden
  - Jens Hartmann
  - Claudia Kammann
  - David P. Keller
  - Florian Kraxner
  - William Lamb
  - Niall Mac Dowell
  - Finn Müller-Hansen
  - Gregory Nemet
  - Benedict Probst
  - Phil Renforth
  - Tim Repke
  - Wilfried Rickels
  - Ingrid Schulte
  - Pete Smith
  - Stephen Smith
  - Daniela Thrän
  - Mijndert van der Spek
  - Jan Minx
date: 2024-03-13T00:00:00Z
doi: "10.21203/rs.3.rs-4109712/v1"

# Schedule page publish date (NOT publication's date).
publishDate: 2024-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "Nature Portfolio"
publication_short: ""

abstract: "Carbon dioxide removal (CDR) is a critical component of any strategy to limit global warming to well below 2°C and rapidly gaining attention in climate research and policymaking. Despite its importance, there have been few attempts to systematically evaluate the scientific evidence on CDR. Here we use an approach rooted in artificial intelligence to produce a comprehensive systematic map of the CDR literature. In particular, we hand-label 5,339 documents to train machine learning classifiers with high levels of precision and recall to identify a total of 28,976 CDR studies across different technology domains and disciplines published in the period 1990-2022 which is at least 2-3 times more than previous studies suggested. We paint a granular picture of available CDR research in terms of the CDR methods studied, the geographical focus of research, the research method applied, and the broad area of research. The field has grown considerably faster than the climate change literature as a whole. This is driven mainly by the rapid expansion of literature on biochar, which made up about 62% of CDR publications in 2022. Beyond this stark concentration of CDR research on a few individual CDR methods, we find that most studies (86%) focus on improving the CDR methods themselves, but there is little research on their societal implications and ethical foundations. Citations patterns from the most recent IPCC report strongly differ from publication patterns on CDR in terms of its attention to CDR methods, research design and methodological context, as does attention to CDR methods in policy and practice in terms of real-world deployments, patenting activity, as well as public investments. As the importance of CDR grows for meeting the Paris climate goals, we believe that the accompanying literature database will be of additional value for the upcoming IPCC assessment, but also for science, policy and practice."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: 
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
