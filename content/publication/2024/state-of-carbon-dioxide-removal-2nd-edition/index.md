
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The State of Carbon Dioxide Removal – 2nd Edition"
authors: 
  - S. M. Smith
  - O. Geden
  - G. Nemet
  - M. Gidden
  - William Lamb
  - C. Powis
  - R. Bellamy
  - Max Callaghan
  - A. Cowie
  - E. Cox
  - S. Fuss
  - T. Gasser
  - G. Grassi
  - J. Greene
  - Sarah Lück
  - A. Mohan
  - Finn Müller-Hansen
  - G. Peters
  - Y. Pratama
  - Tim Repke
  - K. Riahi
  - F. Schenuit
  - J. Steinhauser
  - J. Strefler
  - J. M. Valenzuela
  - Jan Minx
date: 2024-06-04T00:00:00Z
doi: "https://osf.io/f85qj/Y2"

# Schedule page publish date (NOT publication's date).
publishDate: 2024-06-04

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: "www.stateofcdr.org"
publication_short: ""

abstract: "Carbon dioxide removal (CDR) is human activity that captures CO₂ from the atmosphere and stores it for decades to millennia. Alongside rapidly reducing greenhouse gas emissions, the scaling up of novel CDR and the expansion of land-based CDR are urgent priorities if we are to meet the temperature goal of the Paris Agreement. There are many CDR methods, which cover a variety of ways to capture and store CO₂. These methods have different levels of readiness, potential and durability. Each method has sustainability risks that could limit its long-term deployment."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
