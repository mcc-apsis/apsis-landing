---
title: "PROTOCOL: Behavioral, information and monetary interventions to reduce energy consumption in households: A “living” systematic review"
authors:
  - Tarun Khanna
  - Diana Danilenko
  - Mark Andor
  - Max Callaghan
  - Julian Elliott
  - Tim Repke
  - Luke Smith
  - Jorge Sanchez
  - Tanvi Bhumika
  - Jan Minx
date: 2024-07-10T00:00:00Z
doi: "10.1002/cl2.1424"

# Schedule page publish date (NOT publication's date).
publishDate: 2024-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "Campbell reviews"
publication_short: ""

abstract: "This is the protocol for a Campbell systematic review. The objectives are as follows: Our proposed systematic review and meta-analysis will integrate the evidence available from all sources to answer the following questions: (1) to what extent can information, behavioral and monetary interventions reduce energy consumption of households in residential buildings? (average treatment effect of interventions) (2) what is the relative effectiveness of interventions? (account for heterogeneity in treatment effects across and within studies) (3) how effective are combinations of different interventions?"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: 
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
