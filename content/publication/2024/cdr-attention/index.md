---
title: "Growing online attention and positive sentiments towards carbon dioxide removal"
authors:
  - Tim Repke
  - Finn Müller-Hansen
  - Emily Cox
  - Jan Minx
date: 2024-01-18T00:00:00Z
doi: "10.21203/rs.3.rs-3788237/v1"

# Schedule page publish date (NOT publication's date).
publishDate: 2024-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "Communications Earth & Environment"
publication_short: ""

abstract: "Scaling up CO2 removal is crucial to achieve net-zero targets and limit global warming.
To engage with publics and ensure a social licence to deploy large-scale carbon dioxide removal (CDR), better understanding of public perceptions of these technologies is necessary.
Here, we analyse attention and sentiments towards ten CDR methods using Twitter data from 2010 to 2022.
Attention towards CDR has grown exponentially, particularly in recent years.
Overall, the discourse on CDR has become more positive, except for BECCS.
Conventional CDR methods are the most discussed and receive more positive sentiments.
Various types of users engage with CDR on Twitter to different degrees:
While users posting little about CDR pay more attention to methods with biological sinks, frequently engaged users focus more on novel CDR methods.
Our results complement survey studies by showing how awareness grows and perceptions change over time."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: publication/2024/cdr-attention/twitter_cdr.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: 
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
