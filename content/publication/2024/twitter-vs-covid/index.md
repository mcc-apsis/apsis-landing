---
title: "How global crises compete for our attention: Insights from 13.5 million tweets on climate change during COVID-19"
authors:
  - Tim Repke
  - Max Callaghan
  - William Lamb
  - Sarah Lück
  - Finn Müller-Hansen
  - Jan Minx
date: 2024-08-01T00:00:00Z
doi: "10.1016/j.erss.2024.103668"

# Schedule page publish date (NOT publication's date).
publishDate: 2024-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Energy Research & Social Science"
publication_short: ""

abstract: "The COVID-19 pandemic disrupted peoples’ daily lives and dominated the public discourse. It thus displaced people’s attention to and concerns about climate change. We analyse 13.5 million tweets by 3.2 million distinct users on climate change posted before and after the onset of the pandemic (2018–2021) and show that attention to climate dropped substantially in 2020 with the onset of the pandemic. While research has helped to explain this drop in the context of issue attention theory, our analysis highlights a remarkable recovery in attention in 2021 towards pre-pandemic levels. Moreover, our large-scale, transformer-based text analysis reveals important thematic shifts during this period. In particular, we show a sustained drop in attention to activist movements and subsequently an increased focus on climate causes and climate solutions. Activist movements, such as the school protests that have mobilized millions around the globe in 2019, have measurably lost traction on Twitter. However, in parts due to increased awareness of causes and solutions, the climate change discourse in general recovered from the COVID-19 pandemic."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

#url_pdf: 
url_pdf: "publication/2024/twitter-vs-covid/twitter_covid_climate.pdf"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: "https://www.sciencedirect.com/science/article/pii/S2214629624002597"
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
