---
title: "Protocol: Carbon pricing outcomes – An Evidence and Gap Map of the ex-post literature"
authors:
  - Niklas Döbbeling-Hildebrandt
  - Tim Repke
  - Jan Minx
date: 2024-05-17T00:00:00Z
doi: "10.17605/OSF.IO/JWB2R"

# Schedule page publish date (NOT publication's date).
publishDate: 2024-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "OSF"
publication_short: ""

abstract: "Around the world there are more than 70 carbon pricing policies implemented to date, with the earliest being implemented in 1990. After more than 30 years of experience with the policy, thousands of empirical policy assessments have been conducted. In this study we aim to provide the first systematic overview of this empirical literature to foster further evidence synthesis and policy learning. We aim to construct an Evidence and Gap Map listing primary studies and systematic reviews, categorised by the studied policy scheme and policy outcomes. We use a broad search providing more than 40,000 scientific publications and apply a computer-assisted literature screening to identify all relevant articles."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source: 
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
