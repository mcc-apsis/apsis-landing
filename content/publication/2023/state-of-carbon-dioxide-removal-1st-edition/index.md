
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The State of Carbon Dioxide Removal – 1st Edition"
authors: 
  - S. M. Smith
  - O. Geden
  - G. Nemet
  - M. Gidden
  - William Lamb
  - C. Powis
  - R. Bellamy
  - Max Callaghan
  - A. Cowie
  - E. Cox
  - S. Fuss
  - T. Gasser
  - G. Grassi
  - J. Greene
  - Sarah Lück
  - A. Mohan
  - Finn Müller-Hansen
  - G. Peters
  - Y. Pratama
  - Tim Repke
  - K. Riahi
  - F. Schenuit
  - J. Steinhauser
  - J. Strefler
  - J. M. Valenzuela
  - Jan Minx
date: 2023-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "www.stateofcdr.org"
publication_short: ""

abstract: "Scaling up Carbon Dioxide Removal (CDR) is an urgent priority, as are efforts to rapidly reduce emissions, if we are to meet the temperature goal of the Paris Agreement. Scenarios for limiting warming to well below 2°C involve removing hundreds of billions of tonnes of carbon dioxide (CO2) from the atmosphere over the course of the century. Drawing together analysis across several key areas, this report is the first comprehensive global assessment of the current state of CDR."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
