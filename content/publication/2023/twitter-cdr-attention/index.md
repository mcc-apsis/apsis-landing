---
title: "Attention, sentiments and emotions towards emerging climate technologies on Twitter"
authors:
  - Finn Müller-Hansen
  - Tim Repke
  - Chad M. Baum
  - Elina Brutschin
  - Max Callaghan
  - Ramit Debnath
  - William Lamb
  - Sean Low
  - Sarah Lück
  - Cameron Roberts
  - Benjamin K. Sovacool
  - Jan Minx

date: 2023-12-01T00:00:00Z
doi: "10.1016/j.gloenvcha.2023.102765"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Global Environmental Change"
publication_short: ""

abstract: "Public perception of emerging climate technologies, such as greenhouse gas removal (GGR) and solar radiation management (SRM), will strongly influence their future development and deployment. Studying perceptions of these technologies with traditional survey methods is challenging, because they are largely unknown to the public. Social media data provides a complementary line of evidence by allowing for retrospective analysis of how individuals share their unsolicited opinions. Our large-scale, comparative study of 1.5 million tweets covers 16 GGR and SRM technologies and uses state-of-the-art deep learning models to show how attention, and expressions of sentiment and emotion developed between 2006 and 2021. We find that in recent years, attention has shifted from general geoengineering themes to specific GGR methods. On the other hand, there is little attention to specific SRM technologies and they often coincide with conspiracy narratives. Sentiments and emotions in GGR tweets tend to be more positive, particularly for methods perceived to be natural, but are more negative when framed in the geoengineering context."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

#url_pdf: 
url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
