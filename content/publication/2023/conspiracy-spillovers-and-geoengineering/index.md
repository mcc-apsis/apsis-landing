
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Conspiracy spillovers and geoengineering"
authors: 
  - Ramit Debnath
  - David M Reiner
  - Benjamin Sovacool
  - Finn Müller-Hansen
  - Tim Repke
  - R. M. Alvarez
  - S. Fitzgerald
date: 2023-02-01T00:00:00Z
doi: "10.1016/j.isci.2023.106166"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "iScience"
publication_short: ""

abstract: "Geoengineering techniques such as solar radiation management (SRM) could be part of a future technology portfolio to limit global temperature change. However, there is public opposition to research and deployment of SRM technologies. We use 814,924 English-language tweets containing #geoengineering globally over 13 years (2009–2021) to explore public emotions, perceptions, and attitudes toward SRM using natural language processing, deep learning, and network analysis. We find that specific conspiracy theories influence public reactions toward geoengineering, especially regarding “chemtrails” (whereby airplanes allegedly spray poison or modify weather through contrails). Furthermore, conspiracies tend to spillover, shaping regional debates in the UK, USA, India, and Sweden and connecting with broader political considerations. We also find that positive emotions rise on both the global and country scales following events related to SRM governance, and negative and neutral emotions increase following SRM projects and announcements of experiments. Finally, we also find that online toxicity shapes the breadth of spillover effects, further influencing anti-SRM views."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
