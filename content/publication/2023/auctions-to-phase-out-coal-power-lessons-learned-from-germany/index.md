
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Auctions to phase out coal power: Lessons learned from Germany"
authors: 
  - Silvana Tiedemann
  - Finn Müller-Hansen
date: 2023-03-01T00:00:00Z
doi: "10.1016/j.enpol.2022.113387"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Energy Policy"
publication_short: ""

abstract: "This study assesses the extent to which auctions for compensation payments are a suitable policy instrument for ending coal-fired power generation at minimum cost and thus achieving national climate targets. Germany is the first country to apply such a market-based mechanism. Evaluating the effectiveness and efficiency of the auction, we find that the first five of seven auction rounds will retire 10 GW of coal-fired capacity at a cost of 68 ± 5 EUR/kW, corresponding to an additional carbon price of 2.4 ± 0.2 EUR/tCO2. The possibility of administratively shutting down power plants from 2024 and a decreasing ceiling price have ensured that average compensation payments are well below the ceiling price, and low compared to other policies, even though there was no competition in two of five auction rounds. As the government cancels the freed emission allowances, the policy will result in lower emissions, even though the carbon intensity of the German coal power fleet increased slightly by 2%. Thus, the German auctions can serve as a model for national phase-out strategies in countries with similar institutional frameworks and provide a reference case for integrating conflicting policy objectives into auctions."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
