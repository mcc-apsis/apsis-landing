
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Mapping the climate change challenge"
authors: 
  - S. Hallegatte
  - J. Rogelj
  - M. Allen
  - L. Clarke
  - O. Edenhofer
  - C. Field
  - P. Friedlingstein
  - Line Van Kesteren
  - R. Knutti
  - K. Mach
  - M. Mastrandrea
  - A. Michel
  - Jan Minx
  - M. Oppenheimer
  - G. Plattner
  - K. Riahi
  - M. Schaeffer
  - T. Stocker
  - D. P. Vuuren
date: 2016-07-01T00:00:00Z
doi: "10.1038/NCLIMATE3057"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change 6, 663–668 (2016)"
publication_short: ""

abstract: "Discussions on a long-term global goal to limit climate change, in the form of an upper limit to warming, were only partially resolved at the United Nations Framework Convention on Climate Change negotiations in Paris, 2015. Such a political agreement must be informed by scientific knowledge. One way to communicate the costs and benefits of policies is through a mapping that systematically explores the consequences of different choices. Such a multi-disciplinary effort based on the analysis of a set of scenarios helped structure the IPCC AR5 Synthesis Report. This Perspective summarizes this approach, reviews its strengths and limitations, and discusses how decision-makers can use its results in practice. It also identifies research needs that can facilitate integrated analysis of climate change and help better inform policy-makers and the public."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://dspace.library.uu.nl/bitstream/handle/1874/339478/Mapping.pdf?sequence=1&isAllowed=y
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
