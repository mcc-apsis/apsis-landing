
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Shared Socio-Economic Pathways of the Energy Sector – Quantifying the Narratives"
authors: 
  - N. Bauer
  - K. Calvin
  - J. Emmerling
  - Oliver Fricko
  - S. Fujimori
  - Jérôme Hilaire
  - J. Eom
  - V. Krey
  - E. Kriegler
  - I. Mouratiadou
  - H. D. Boer
  - M. Berg
  - Samuel Carrara
  - V. Daioglou
  - L. Drouet
  - J. Edmonds
  - D. Gernaat
  - P. Havlík
  - N. Johnson
  - David Klein
  - P. Kyle
  - G. Marangoni
  - T. Masui
  - R. Pietzcker
  - M. Strubegger
  - M. Wise
  - K. Riahi
  - D. P. Vuuren
date: 2016-01-01T00:00:00Z
doi: "10.1016/J.GLOENVCHA.2016.07.006"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Global Environmental Change"
publication_short: ""

abstract: "Energy is crucial for supporting basic human needs, development and well-being. The future evolution of the scale and character of the energy system will be fundamentally shaped by socioeconomic conditions and drivers, available energy resources, technologies of energy supply and transformation, and end-use energy demand. However, because energy-related activities are significant sources of greenhouse gas (GHG) emissions and other environmental and social externalities, energy system development will also be influenced by social acceptance and strategic policy choices. All of these uncertainties have important implications for many aspects of economic and environmental sustainability, and climate change in particular. In the Shared-Socioeconomic Pathway (SSP) framework these uncertainties are structured into five narratives, arranged according to the challenges to climate change mitigation and adaptation. In this study we explore future energy sector developments across the five SSPs using Integrated Assessment Models (IAMs), and we also provide summary output and analysis for selected scenarios of global emissions mitigation policies. The mitigation challenge strongly corresponds with global baseline energy sector growth over the 21st century, which varies between 40% and 230% depending on final energy consumer behavior, technological improvements, resource availability and policies. The future baseline CO2-emission range is even larger, as the most energy-intensive SSP also incorporates a comparatively high share of carbon-intensive fossil fuels, and vice versa. Inter-regional disparities in the SSPs are consistent with the underlying socioeconomic assumptions; these differences are particularly strong in the SSPs with large adaptation challenges, which have little inter-regional convergence in long-term income and final energy demand levels. The scenarios presented do not include feedbacks of climate change on energy sector development. The energy sector SSPs with and without emissions mitigation policies are introduced and analyzed here in order to contribute to future research in climate sciences, mitigation analysis, and studies on impacts, adaptation and vulnerability."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
