
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Den Klimawandel stoppen. Es gibt nicht zu wenig, sondern zu viel fossile Ressourcen - sie müssen in der Erde bleiben,"
authors: 
  - O. Edenhofer
  - Christian Flachsland
  - M. Jakob
  - Jérôme Hilaire
date: 2016-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Patenge, M., Beck, R., Luber, M. (eds.): Schöpfung bewahren - Theologie und Kirche als Implusgeber für eine nachhaltige Entwicklung, Verlag Friedrich Pustet, 30-34."
publication_short: ""

abstract: "Edenhofer, O., Flachsland, C., Hilaire, J., Jakob, M. (2016): Den Klimawandel stoppen. Es gibt nicht zu wenig, sondern zu viel fossile Ressourcen - sie müssen in der Erde bleiben, In: Patenge, M., Beck, R., Luber, M. (eds.): Schöpfung bewahren - Theologie und Kirche als Implusgeber für eine nachhaltige Entwicklung, Verlag Friedrich Pustet, 30-34."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
