
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Which countries avoid carbon-intensive development?"
authors: 
  - William Lamb
date: 2016-09-10T00:00:00Z
doi: "10.1016/J.JCLEPRO.2016.04.148"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Journal of Cleaner Production"
publication_short: ""

abstract: "This paper explores the underlying development outcomes and cumulative emissions trajectories of 20 middle-income countries from Eastern Europe, Latin America, North Africa and South Asia. First, well-being outcomes are assessed, defined in terms of access to education, democratic and legal rights, and the infrastructures that support physical health. Second, emissions trajectories are estimated to 2050, taking into account current trends in energy consumption and carbon intensity, a likely start-date for stringent climate policy arising from the Paris Agreement (2020), and maximum feasible rates of mitigation. Comparing these estimates to a per capita allocation from the global carbon budget associated with 2 °C, ten countries have low-carbon development trends that will not exceed their allocation. Of these, Costa Rica and Uruguay are achieving very high well-being outcomes, while many more are delivering good outcomes in at least two domains of human need. However, most are seriously deficient in terms of social well-being (education, democratic and legal rights). These results call into question the socio-economic convergence of developing countries with industrialised countries; but they also reaffirm the low-emissions cost of extending good infrastructure access and physical health outcomes to all, demonstrated by the existence of multiple countries that continue to avoid carbon-intensive development."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://refubium.fu-berlin.de/bitstream/fub188/18044/1/WLambxJCLPxWhichxcountriesxavoidxcarbon-intensivexdevelopment.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
