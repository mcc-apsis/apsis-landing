
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Fossil-fueled development (SSP5): An energy and resource intensive scenario for the 21st century"
authors: 
  - E. Kriegler
  - N. Bauer
  - A. Popp
  - Florian Humpenöder
  - M. Leimbach
  - J. Strefler
  - Lavinia Baumstark
  - B. Bodirsky
  - Jérôme Hilaire
  - David Klein
  - I. Mouratiadou
  - I. Weindl
  - C. Bertram
  - J. Dietrich
  - G. Luderer
  - M. Pehl
  - R. Pietzcker
  - F. Piontek
  - H. Lotze-Campen
  - A. Biewald
  - M. Bonsch
  - A. Giannousakis
  - Ulrich Kreidenweis
  - C. Müller
  - S. Rolinski
  - Anselm Schultes
  - Jana Schwanitz
  - M. Stevanović
  - K. Calvin
  - J. Emmerling
  - S. Fujimori
  - O. Edenhofer
date: 2016-01-01T00:00:00Z
doi: "10.1016/J.GLOENVCHA.2016.05.015"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Global Environmental Change"
publication_short: ""

abstract: "This paper presents a set of energy and resource intensive scenarios based on the concept of Shared Socio-Economic Pathways (SSPs). The scenario family is characterized by rapid and fossil-fueled development with high socio-economic challenges to mitigation and low socio-economic challenges to adaptation (SSP5). A special focus is placed on the SSP5 marker scenario developed by the REMIND-MAgPIE integrated assessment modeling framework. The SSP5 baseline scenarios exhibit very high levels of fossil fuel use, up to a doubling of global food demand, and up to a tripling of energy demand and greenhouse gas emissions over the course of the century, marking the upper end of the scenario literature in several dimensions. These scenarios are currently the only SSP scenarios that result in a radiative forcing pathway as high as the highest Representative Concentration Pathway (RCP8.5). This paper further investigates the direct impact of mitigation policies on the SSP5 energy, land and emissions dynamics confirming high socio-economic challenges to mitigation in SSP5. Nonetheless, mitigation policies reaching climate forcing levels as low as in the lowest Representative Concentration Pathway (RCP2.6) are accessible in SSP5. The SSP5 scenarios presented in this paper aim to provide useful reference points for future climate change, climate impact, adaption and mitigation analysis, and broader questions of sustainable development."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
