
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Urban infrastructure choices structure climate solutions"
authors: 
  - F. Creutzig
  - P. Ágoston
  - Jan Minx
  - J. Canadell
  - R. Andrew
  - Corinne Le Quéré
  - G. Peters
  - Ayyoob Sharifi
  - Yyoshiki Yamagata
  - S. Dhakal
date: 2016-12-01T00:00:00Z
doi: "10.1038/NCLIMATE3169"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "Cities are becoming increasingly important in combatting climate change, but their overall role in global solution pathways remains unclear. Here we suggest structuring urban climate solutions along the use of existing and newly built infrastructures, providing estimates of the mitigation potential."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://pub.cicero.oslo.no/cicero-xmlui/bitstream/11250/2484051/1/Urban%2binfrastructure%2bchoices%2bstructure%2bclimate%2bsolutions.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
