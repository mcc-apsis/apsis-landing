
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Assessing global fossil fuel availability in a scenario framework"
authors: 
  - N. Bauer
  - Jérôme Hilaire
  - R. Brecha
  - J. Edmonds
  - K. Jiang
  - E. Kriegler
  - H. Rogner
  - F. Sferra
date: 2016-09-15T00:00:00Z
doi: "10.1016/J.ENERGY.2016.05.088"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Energy, Volume 111, 15 September 2016, Pages 580–592"
publication_short: ""

abstract: "This study assesses global, long-term economic availability of coal, oil and gas within the Shared Socio-economic Pathway (SSP) scenario framework considering alternative assumptions as to highly uncertain future developments of technology, policy and the economy. Diverse sets of trajectories are formulated varying the challenges to mitigation and adaptation of climate change. The potential CO2 emissions from fossil fuels make it a crucial element subject to deep uncertainties. The analysis is based on a well-established dataset of cost-quantity combinations that assumes favorable techno-economic developments, but ignores additional constraints on the extraction sector. This study significantly extends the analysis by specifying alternative assumptions for the fossil fuel sector consistent with the SSP scenario families and applying these filters (mark-ups and scaling factors) to the original dataset, thus resulting in alternative cumulative fossil fuel availability curves. In a Middle-of-the-Road scenario, low cost fossil fuels embody carbon consistent with a RCP6.0 emission profile, if all the CO2 were emitted freely during the 21st century. In scenarios with high challenges to mitigation, the assumed embodied carbon in low-cost fossil fuels can trigger a RCP8.5 scenario; low mitigation challenges scenarios are still consistent with a RCP4.5 scenario."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
