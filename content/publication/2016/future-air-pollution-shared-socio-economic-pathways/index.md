
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Future air pollution in the Shared Socio-economic Pathways"
authors: 
  - Shilpa Rao
  - Z. Klimont
  - Steven J. Smith
  - R. Dingenen
  - F. Dentener
  - L. Bouwman
  - K. Riahi
  - M. Amann
  - B. Bodirsky
  - D. P. Vuuren
  - L. Reis
  - K. Calvin
  - L. Drouet
  - Oliver Fricko
  - S. Fujimori
  - D. Gernaat
  - P. Havlík
  - M. Harmsen
  - T. Hasegawa
  - C. Heyes
  - Jérôme Hilaire
  - G. Luderer
  - T. Masui
  - E. Stehfest
  - J. Strefler
  - S. V. D. Sluis
  - M. Tavoni
date: 2016-01-01T00:00:00Z
doi: "10.1016/J.GLOENVCHA.2016.05.012"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Global Environmental Change"
publication_short: ""

abstract: "Emissions of air pollutants such as sulfur and nitrogen oxides and particulates have significant health impacts as well as effects on natural and anthropogenic ecosystems. These same emissions also can change atmospheric chemistry and the planetary energy balance, thereby impacting global and regional climate. Long-term scenarios for air pollutant emissions are needed as inputs to global climate and chemistry models, and for analysis linking air pollutant impacts across sectors. In this paper we present methodology and results for air pollutant emissions in Shared Socioeconomic Pathways (SSP) scenarios. We first present a set of three air pollution narratives that describe high, central, and low pollution control ambitions over the 21st century. These narratives are then translated into quantitative guidance for use in integrated assessment models. The resulting pollutant emission trajectories under the SSP scenarios cover a wider range than the scenarios used in previous international climate model comparisons. In the SSP3 and SSP4 scenarios, where economic, institutional and technological limitations slow air quality improvements, global pollutant emissions over the 21st century can be comparable to current levels. Pollutant emissions in the SSP1 scenarios fall to low levels due to the assumption of technological advances and successful global action to control emissions."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
