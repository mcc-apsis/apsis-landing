
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "2°C and SDGs: United they stand, divided they fall?"
authors: 
  - Christoph von Stechow
  - Jan Minx
  - K. Riahi
  - J. Jewell
  - D. McCollum
  - Max Callaghan
  - C. Bertram
  - G. Luderer
  - G. Baiocchi
date: 2016-03-16T00:00:00Z
doi: "10.1088/1748-9326/11/3/034022"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "The adoption of the Sustainable Development Goals (SDGs) and the new international climate treaty could put 2015 into the history books as a defining year for setting human development on a more sustainable pathway. The global climate policy and SDG agendas are highly interconnected: the way that the climate problem is addressed strongly affects the prospects of meeting numerous other SDGs and vice versa. Drawing on existing scenario results from a recent energy-economy-climate model inter-comparison project, this letter analyses these synergies and (risk) trade-offs of alternative 2 °C pathways across indicators relevant for energy-related SDGs and sustainable energy objectives. We find that limiting the availability of key mitigation technologies yields some co-benefits and decreases risks specific to these technologies but greatly increases many others. Fewer synergies and substantial trade-offs across SDGs are locked into the system for weak short-term climate policies that are broadly in line with current Intended Nationally Determined Contributions (INDCs), particularly when combined with constraints on technologies. Lowering energy demand growth is key to managing these trade-offs and creating synergies across multiple energy-related SD dimensions. We argue that SD considerations are central for choosing socially acceptable 2 °C pathways: the prospects of meeting other SDGs need not dwindle and can even be enhanced for some goals if appropriate climate policy choices are made. Progress on the climate policy and SDG agendas should therefore be tracked within a unified framework."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
