
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Analyse: Maßnahmen für energiesparendes Verhalten im Wohnsektor"
authors: 
  - T. Khanna
  - Klaas Miersch
  - F. Creutzig
  - R. Meyer
  - J. Karras
  - G. Reh
  - Jan Minx
date: 2022-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Ariadne-Analyse"
publication_short: ""

abstract: "Von Informationskampagnen bis hin zu monetären Anreizen, von Selbstverpflichtungen bis hin zu Spielen:  5-6 Prozent des Energieverbrauchs von Haushalten können kurz- und mittelfristig durch Verhaltensinterventionen gesenkt werden und so helfen, die Energiekrise abzufedern. Das zeigt eine neue Analyse, für die Ariadne-Forschende des Mercator Research Institute on Global Commons and Climate Change, des Fraunhofer-Instituts für Solare Energiesysteme und des Instituts für Klimaschutz, Energie und Mobilität mehr als 100 Studien aus über 25 Ländern ausgewertet haben. Das Papier diskutiert aktuelle wissenschaftliche Erkenntnisse, analysiert den rechtlichen Rahmen sowie den Stand der Implementierung in Deutschland und zeigt entsprechende Regulierungsoptionen auf."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
