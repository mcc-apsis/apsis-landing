
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The 2022 report of the Lancet Countdown on health and climate change: health at the mercy of fossil fuels"
authors: 
  - Marina Romanello
  - C. Napoli
  - P. Drummond
  - Carole Green
  - H. Kennard
  - P. Lampard
  - Daniel Scamman
  - N. Arnell
  - Sonja Ayeb-Karlsson
  - L. B. Ford
  - K. Belesova
  - K. Bowen
  - W. Cai
  - Max Callaghan
  - D. Campbell-Lendrum
  - Jonathan Chambers
  - K. Daalen
  - C. Dalin
  - N. Dasandi
  - Shouro Dasgupta
  - M. Davies
  - P. Dominguez-Salas
  - Robert Dubrow
  - K. Ebi
  - M. Eckelman
  - P. Ekins
  - Luis E. Escobar
  - L. Georgeson
  - Hilary Graham
  - Samuel H Gunther
  - I. Hamilton
  - Yun Hang
  - R. Hänninen
  - S. Hartinger
  - Kehan He
  - J. Hess
  - Shih-Che Hsu
  - Slava Jankin
  - Louis Jamart
  - Ollie Jay
  - I. Kelman
  - G. Kiesewetter
  - P. Kinney
  - T. Kjellstrom
  - D. Kniveton
  - J. Lee
  - B. Lemke
  - Yang Liu
  - Zhao Liu
  - M. Lott
  - Martin Lotto Batista
  - R. Lowe
  - F. MacGuire
  - M. Sewe
  - J. Martínez-Urtaza
  - M. Maslin
  - Lucy McAllister
  - Alice McGushin
  - C. McMichael
  - Z. Mi
  - J. Milner
  - K. Minor
  - Jan Minx
  - N. Mohajeri
  - M. Moradi-Lakeh
  - K. Morrissey
  - Simon Munzert
  - K. Murray
  - T. Neville
  - M. Nilsson
  - Nick Obradovich
  - M. O'Hare
  - T. Oreszczyn
  - M. Otto
  - F. Owfi
  - O. Pearman
  - M. Rabbaniha
  - Elizabeth J. Z. Robinson
  - J. Rocklöv
  - Renee N. Salas
  - J. Semenza
  - J. Sherman
  - Liuhua Shi
  - J. Shumake-Guillemot
  - Grant Silbert
  - M. Sofiev
  - M. Springmann
  - Jennifer D Stowell
  - M. Tabatabaei
  - Jonathon Taylor
  - J. Triñanes
  - Fabian Wagner
  - P. Wilkinson
  - M. Winning
  - Marisol Yglesias-González
  - Shihui Zhang
  - P. Gong
  - Hugh Montgomery
  - A. Costello
date: 2022-10-01T00:00:00Z
doi: "10.1016/S0140-6736(22)01540-9"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2", "2"]

# Publication name and optional abbreviated publication name.
publication: "The Lancet"
publication_short: ""

abstract: "None"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://gala.gre.ac.uk/id/eprint/37919/3/37919_DOMINGUEZ_SALAS_The_2022_report_of_the_Lancet_Countdown.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
