
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The German coal debate on Twitter: Reactions to a corporate policy process"
authors: 
  - Finn Müller-Hansen
  - Yuan Ting Lee
  - Max Callaghan
  - Slava Jankin
  - Jan Minx
date: 2022-10-01T00:00:00Z
doi: "10.1016/j.enpol.2022.113178"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Energy Policy"
publication_short: ""

abstract: "Phasing out coal is a prerequisite to achieving the Paris climate mitigation targets. In 2018, the German government established a multi-stakeholder commission with the mandate to negotiate a plan for the national coal phase-out, fueling a continued public debate over the future of coal. This study analyzes the German coal debate on Twitter before, during, and after the session of the so-called Coal Commission, over a period of three years. In particular, we investigate whether and how the work of the commission translated into shared perceptions and sentiments in the public debate on Twitter. We find that the sentiment of the German coal debate on Twitter becomes increasingly negative over time. In addition, the sentiment becomes more polarized over time due to an increase in the use of more negative and positive language. The analysis of retweet networks shows no increase in interactions between communities over time. These findings suggest that the Coal Commission did not further consensus in the coal debate on Twitter. While the debate on social media only represents a section of the national debate, it provides insights for policy-makers to evaluate the interaction of multi-stakeholder commissions and public debates."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
