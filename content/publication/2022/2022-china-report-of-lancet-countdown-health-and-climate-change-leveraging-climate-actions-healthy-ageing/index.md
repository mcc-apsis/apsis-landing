
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The 2022 China report of the Lancet Countdown on health and climate change: leveraging climate actions for healthy ageing"
authors: 
  - W. Cai
  - Chi Zhang
  - Shihui Zhang
  - Yuqi Bai
  - Max Callaghan
  - Nan Chang
  - Bin Chen
  - Huiqi Chen
  - Liangliang Cheng
  - Xueqin Cui
  - Hancheng Dai
  - B. Danna
  - Wenxuan Dong
  - Weicheng Fan
  - Xiaoyi Fang
  - Tong Gao
  - Yang Geng
  - D. Guan
  - Yixin Hu
  - Junyi Hua
  - Cunrui Huang
  - Hong Huang
  - Jianbin Huang
  - Linlang Jiang
  - Qiaolei Jiang
  - Xiaopeng Jiang
  - H. Jin
  - G. Kiesewetter
  - Lu Liang
  - B. Lin
  - Hualiang Lin
  - Huan Liu
  - Qiyong Liu
  - Tao Liu
  - Xiaobo Liu
  - Xinyuan Liu
  - Zhao Liu
  - Zhuiguo Liu
  - Shuhan Lou
  - Chenxi Lu
  - Zhenyu Luo
  - W. Meng
  - Hui Miao
  - Chao Ren
  - Marina Romanello
  - Wolfgang Schöpp
  - Jing Su
  - Xu-Sheng Tang
  - Can Wang
  - Qiong Wang
  - Laura Warnecke
  - Sanmei Wen
  - W. Winiwarter
  - Yang Xie
  - Bing Xu
  - Yu Yan
  - Xiu Yang
  - Fanghong Yao
  - Le Yu
  - Jiacan Yuan
  - Yiping Zeng
  - Jing Zhang
  - Luo Zhang
  - Rui Zhang
  - Shangcheng Zhang
  - Shaohui Zhang
  - Qidong Zhao
  - Dashan Zheng
  - Hao Zhou
  - Jingbo Zhou
  - M. Fung
  - Yongde Luo
  - P. Gong
date: 2022-10-01T00:00:00Z
doi: "10.1016/S2468-2667(22)00224-9"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2", "2"]

# Publication name and optional abbreviated publication name.
publication: "The Lancet Public Health"
publication_short: ""

abstract: "A health-friendly, climate resilient, and carbon-neutral pathway would deliver major benefits to people's health and wellbeing in China, especially for older populations, while simultaneously promoting high-quality development in the long run."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
