
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The 2022 Europe report of the Lancet Countdown on health and climate change: towards a climate resilient future"
authors: 
  - K. V. van Daalen
  - Marina Romanello
  - J. Rocklöv
  - J. Semenza
  - C. Tonne
  - A. Markandya
  - N. Dasandi
  - Slava Jankin
  - Hicham Achebak
  - J. Ballester
  - Hannah Bechara
  - Max Callaghan
  - J. Chambers
  - Shouro Dasgupta
  - P. Drummond
  - Zia Farooq
  - O. Gasparyan
  - N. González-Reviriego
  - I. Hamilton
  - R. Hänninen
  - A. Kaźmierczak
  - V. Kendrovski
  - H. Kennard
  - G. Kiesewetter
  - S. Lloyd
  - Martin Lotto Batista
  - J. Martínez-Urtaza
  - C. Milà
  - Jan Minx
  - M. Nieuwenhuijsen
  - J. Palamarchuk
  - M. Quijal-Zamorano
  - Elizabeth J. Z. Robinson
  - Daniel Scamman
  - O. Schmoll
  - M. Sewe
  - H. Sjödin
  - M. Sofiev
  - Balakrishnan Solaraju-Murali
  - M. Springmann
  - J. Triñanes
  - J. Anto
  - M. Nilsson
  - R. Lowe
date: 2022-10-01T00:00:00Z
doi: "10.1016/S2468-2667(22)00197-9"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2", "2"]

# Publication name and optional abbreviated publication name.
publication: "The Lancet Public Health"
publication_short: ""

abstract: "In the past few decades, major public health advances have happened in Europe, with drastic decreases in premature mortality and a life expectancy increase of almost 9 years since 1980. European countries have some of the best health-care systems in the world. However, Europe is challenged with unprecedented and overlapping crises that are detrimental to human health and livelihoods and threaten adaptive capacity, including the COVID-19 pandemic, the Russian invasion of Ukraine, the fastest-growing migrant crisis since World War 2, population displacement, environmental degradation, and deepening inequalities. Compared with pre-industrial times, the mean average European surface air temperature increase has been almost 1°C higher than the average global temperature increase, and 2022 was the hottest European summer on record. As the world's third largest economy and a major contributor to global cumulative greenhouse gas emissions, Europe is a key stakeholder in the world's response to climate change and has a global responsibility and opportunity to lead the transition to becoming a low-carbon economy and a healthier, more resilient society."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://www.thelancet.com/article/S2468266722001979/pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
