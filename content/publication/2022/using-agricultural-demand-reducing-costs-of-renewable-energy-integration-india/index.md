
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Using agricultural demand for reducing costs of renewable energy integration in India"
authors: 
  - T. Khanna
date: 2022-05-01T00:00:00Z
doi: "10.1016/j.energy.2022.124385"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Energy"
publication_short: ""

abstract: "While demand response is recognized as a useful tool for integrating renewable electricity, the related literature in developing countries has been limited. Meanwhile, the literature on demand side management (DSM) has ignored the value of agricultural demand as a demand side resource for integration of renewable energy. This article fills the gap by collecting agricultural load data from two distribution utilities in the Indian state of Gujarat and using it in a mixed-integer linear programming model to estimate the flexibility provided by agricultural DSM to the power system. Using a flexible load representation, the model chooses the optimal periods for agricultural supply subject to the constraints of meeting the irrigation needs of farmers and the marginal cost of electricity. This analysis shows that management of agricultural demand already reduces system costs by 4% or USD 6.09 per MWh of agricultural consumption. Going forward, with high shares of solar generation, shifting agricultural demand to daytime hours increases system flexibility. It reduces renewables curtailment by 4–7%, limits cycling costs of coal power plants, and reduces system integration costs by 22%. Agricultural DSM could be a cost-effective flexibility option in developing countries where only the least-cost options are economically viable."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
