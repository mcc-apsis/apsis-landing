
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Enhancing the review process in global environmental assessments: The case of the IPCC"
authors: 
  - J. Palutikof
  - S. Boulter
  - Christopher B. Field
  - K. Mach
  - M. Manning
  - M. Mastrandrea
  - L. Meyer
  - Jan Minx
  - J. Pereira
  - G. Plattner
  - S. K. Ribeiro
  - Y. Sokona
  - F. Stadler
  - R. Swart
date: 2023-01-01T00:00:00Z
doi: "10.1016/j.envsci.2022.10.012"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Science & Policy"
publication_short: ""

abstract: "External review is a fundamental component of Global Environmental Assessments, ensuring their processes are comprehensive, objective, open and transparent, and are perceived as such. Here, we focus on review of Intergovernmental Panel on Climate Change (IPCC) Assessment Reports. The review process has received little scrutiny, although review comments and author responses are public. Here we analyse review documents from the Fourth and Fifth Assessments, focusing primarily on Working Group II. We address three questions: Is the review representative? Is it comprehensive? Is it insightful? Overall we found the review process to be fit for purpose, although there are outstanding issues. First, the overwhelming majority of reviewers are from developed countries, although evidence suggests participation by developing country reviewers increased between the Fourth and Fifth Assessments. Second, earlier sections of chapters are more densely reviewed than later ones. This is true even when executive summaries are removed from analysis. In consequence, some sections on specialised topics may escape in-depth review. Thirdly, those review comments which are received make a valid and valuable contribution to the scientific development of chapters. We suggest how outstanding issues could be addressed, including through enhanced reviewer recognition, a wider role for review editors, adherence to mandated page lengths from early in the process, reviewer training, and consistency in reporting to allow systematic evaluation. Making such changes will result in more transparent, consistent and representative processes delivering reviews which effectively contribute to the credibility and legitimacy of future Global Environmental Assessments and, ultimately, their recognition and contribution."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
