
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A review of trends and drivers of greenhouse gas emissions by sector from 1990 to 2018"
authors: 
  - William Lamb
  - T. Wiedmann
  - J. Pongratz
  - R. Andrew
  - M. Crippa
  - J. Olivier
  - Dominik Wiedenhofer
  - Giulio Mattioli
  - Alaa Al Khourdajie
  - Jo House
  - S. Pachauri
  - M. Figueroa
  - Y. Saheb
  - R. Slade
  - K. Hubacek
  - Laixiang Sun
  - S. K. Ribeiro
  - S. Khennas
  - S. de la rue du Can
  - L. Chapungu
  - S. Davis
  - I. Bashmakov
  - Hancheng Dai
  - S. Dhakal
  - Xianchun Tan
  - Y. Geng
  - Baihe Gu
  - Jan Minx
date: 2021-03-12T00:00:00Z
doi: "10.1088/1748-9326/abee4e"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "Global greenhouse gas (GHG) emissions can be traced to five economic sectors: energy, industry, buildings, transport and AFOLU (agriculture, forestry and other land uses). In this topical review, we synthesise the literature to explain recent trends in global and regional emissions in each of these sectors. To contextualise our review, we present estimates of GHG emissions trends by sector from 1990 to 2018, describing the major sources of emissions growth, stability and decline across ten global regions. Overall, the literature and data emphasise that progress towards reducing GHG emissions has been limited. The prominent global pattern is a continuation of underlying drivers with few signs of emerging limits to demand, nor of a deep shift towards the delivery of low and zero carbon services across sectors. We observe a moderate decarbonisation of energy systems in Europe and North America, driven by fuel switching and the increasing penetration of renewables. By contrast, in rapidly industrialising regions, fossil-based energy systems have continuously expanded, only very recently slowing down in their growth. Strong demand for materials, floor area, energy services and travel have driven emissions growth in the industry, buildings and transport sectors, particularly in Eastern Asia, Southern Asia and South-East Asia. An expansion of agriculture into carbon-dense tropical forest areas has driven recent increases in AFOLU emissions in Latin America, South-East Asia and Africa. Identifying, understanding, and tackling the most persistent and climate-damaging trends across sectors is a fundamental concern for research and policy as humanity treads deeper into the Anthropocene."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
