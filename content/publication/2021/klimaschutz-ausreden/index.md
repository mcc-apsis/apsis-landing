
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Klimaschutz-Ausreden"
authors: 
  - S. Levi
  - Finn Müller-Hansen
  - William Lamb
  - G. Mattioli
  - J. Roberts
  - S. Capstick
  - F. Creutzig
  - Jan Minx
  - Trevor Culhane
  - J. Steinberger
date: 2021-01-01T00:00:00Z
doi: "10.30820/9783837978018-89"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Buch ''Climate Action - Psychologie der Klimakrise''"
publication_short: ""

abstract: "Die Klimakrise spitzt sich zu, der Klimawandel wird immer stärker spürbar. Warum gelingt es vielfach trotzdem nicht, dringend notwendige Eindämmungsmaßnahmen einzuleiten und zu handeln? Die Autorinnen und Autoren beleuchten aus psychologischer und interdisziplinärer Sicht die Hindernisse, die einer produktiven Auseinandersetzung mit der Krise im Wege stehen. Sie bieten Inspirationen für den Umgang mit den Herausforderungen des Klimawandels und stellen Grundideen für ein konstruktives und kollektives Handeln dar. Dabei denken sie individuelles Handeln auf gesellschaftlicher Ebene und zeigen, dass jede*r in der Klimakrise wirksam werden und dabei gesund bleiben kann."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
