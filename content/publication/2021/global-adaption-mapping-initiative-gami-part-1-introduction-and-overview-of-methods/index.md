
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The Global Adaption Mapping Initiative (GAMI): Part 1 - Introduction and overview of methods"
authors: 
  - L. Berrang‐Ford
  - A. Lesnikowski
  - A. P. Fischer
  - A. Siders
  - K. Mach
  - A. Thomas
  - Max Callaghan
  - Neal R Haddaway
  - R. Kerr
  - R. Biesbroek
  - K. Bowen
  - D. Deryng
  - Susan Elliott
  - J. Ford
  - Matthias Garschagen
  - E. Gilmore
  - S. Harper
  - Marjolijn Hassnoot
  - T. Lissner
  - S. Lwasa
  - A. Magnan
  - Jan Minx
  - M. Morecroft
  - M. New
  - E. Perez
  - D. Reckien
  - Nick Simpson
  - C. Singh
  - L. Stringer
  - E. Totin
  - C. Trisos
  - M. V. Aalst
date: 2021-02-05T00:00:00Z
doi: "10.21203/RS.3.PEX-1240/V1"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "PROTOCOL"
publication_short: ""

abstract: "
 Context: It is now widely accepted that the climate is changing, and that societal responses will need to be rapid and comprehensive to prevent the most severe impacts. A key milestone in global climate governance is to assess progress on adaptation. To-date, however, there has been negligible robust, systematic synthesis of progress on adaptation or adaptation-relevant responses globally. Aim: The purpose of this review protocol is to outline the methods used by the Global Adaptation Mapping Initiative (GAMI) to systematically review human adaptation responses to climate-related changes that have been documented globally since 2013 in the scientific literature. The broad question underpinning this review is: Are we adapting to climate change? More specifically, we ask ‘what is the evidence relating to human adaptation-related responses that can (or are) directly reducing risk, exposure, and/or vulnerability to climate change?’ This work responds to the recognition of the need for high-level syntheses of adaptation research to inform global and regional climate assessments.Methods: We review scientific literature 2013-2019 to identify documents empirically reporting on observed adaptation-related responses to climate change in human systems that can directly reduce risk. We exclude non-empirical (theoretical & conceptual) literature and adaptation in natural systems that occurs without human intervention. Included documents were coded across a set of questions focused on: Who is responding? What responses are documented? What is the extent of the adaptation-related response? What is the evidence that adaptation-related responses reduce risk, exposure and/or vulnerability? Once articles are coded, we conduct a quality appraisal of the coding and develop ‘evidence packages’ for regions and sectors. We supplement this systematic mapping with an expert elicitation exercise, undertaken to assess bias and validity of insights from included/coded literature vis a vis perceptions of real-world adaptation for global regions and sectors, with associated confidence assessments. Related protocols: This protocol represents Part 1 of a 5-part series outlining the phases of methods for this initiative. Part 1 provides an introduction to the Global Adaptation Mapping Initiative (GAMI) and an overview of methods."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.researchsquare.com/article/pex-1240/latest.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
