
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Face mask use and physical distancing before and after mandatory masking: No evidence on risk compensation in public waiting lines"
authors: 
  - Gyula Seres
  - Anna Balleyer
  - Nicola Cerutti
  - J. Friedrichsen
  - Müge Süer
date: 2020-07-02T00:00:00Z
doi: "10.1016/j.jebo.2021.10.032"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Journal of Economic Behaviour and Organization"
publication_short: ""

abstract: "During the COVID-19 pandemic, the introduction of mandatory face mask usage triggered a heated debate. A major point of controversy is whether community use of masks creates a false sense of security that would diminish physical distancing, counteracting any potential direct benefit from masking. We conducted a randomized field experiment in Berlin, Germany, to investigate how masks affect distancing and whether the mask effect interacts with the introduction of an indoor mask mandate. Joining waiting lines in front of stores, we measured distances kept from the experimenter in two treatment conditions – the experimenter wore a mask in one and no face covering in the other – in two time spans – before and after mask use becoming mandatory in stores. We find no evidence that mandatory masking has a negative effect on distance kept toward a masked person. To the contrary, masks significantly increase distancing and the effect does not differ between the two periods. However, we show that after the mandate distances are shorter in locations where more non-essential stores, which were closed before the mandate, had reopened. We argue that the relaxations in general restrictions that coincided with the mask mandate led individuals to reduce other precautions, like keeping a safe distance."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
