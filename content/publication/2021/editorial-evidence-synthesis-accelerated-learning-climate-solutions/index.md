
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Editorial: Evidence synthesis for accelerated learning on climate solutions"
authors: 
  - L. Berrang‐Ford
  - Friederike Döbbe
  - R. Garside
  - Neal R Haddaway
  - William Lamb
  - Jan Minx
  - W. Viechtbauer
  - V. Welch
  - H. White
date: 2020-12-01T00:00:00Z
doi: "10.1002/cl2.1128"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Campbell Systematic Reviews"
publication_short: ""

abstract: "By signing the Paris Agreement with the aim to limit global warming to well below 2°C relative to preindustrial levels, countries have committed to kickstart the age of climate solutions. This challenge should not be underestimated: It requires turning around a 270‐year‐old trajectory of CO2 emissions growth that started with the industrial revolution (Friedlingstein et al., 2019), and racing towards net zero emissions over the next 3–5 decades (IPCC, 2018). But that is not enough. Because anthropogenic carbon emissions have already caused consequential warming of about 1°C since preindustrial times, there is a further need to reduce vulnerabilities and adapt to climate impacts that cannot be avoided (IPCC, 2014, 2018). All parts of society and the economy will need to play their parts in the transformation towards a climate‐resilient, net‐zero emissions world. It requires nothing less than transformational policies at all levels of governance from local to national and international (IPCC 2014, 2018). Climate policies for mitigation and adaptation have to become the focus in science and policy if we are to have the slightest chance of living up to this challenge."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://onlinelibrary.wiley.com/doi/pdfdirect/10.1002/cl2.1128
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
