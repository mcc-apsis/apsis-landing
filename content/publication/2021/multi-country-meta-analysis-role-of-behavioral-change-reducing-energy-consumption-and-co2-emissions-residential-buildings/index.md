
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A multi-country meta-analysis on the role of behavioral change in reducing energy consumption and CO2 emissions in residential buildings"
authors: 
  - T. Khanna
  - G. Baiocchi
  - Max Callaghan
  - F. Creutzig
  - Horia Guias
  - N. Haddaway
  - Lion Hirth
  - Aneeque Javaid
  - Nicolas Koch
  - Sonja Laukemper
  - A. Löschel
  - María del Mar Zamora
  - Jan Minx
date: 2021-07-26T00:00:00Z
doi: "10.1038/s41560-021-00866-x"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Nature Energy"
publication_short: ""

abstract: "Despite the importance of evaluating all mitigation options to inform policy decisions addressing climate change, a comprehensive analysis of household-scale interventions and their emissions reduction potential is missing. Here, we address this gap for interventions aimed at changing individual households’ usage of existing equipment, such as monetary incentives or feedback. We perform a machine learning-assisted systematic review and meta-analysis to comparatively assess the effectiveness of these interventions in reducing energy demand in residential buildings. We extract 360 individual effect sizes from 122 studies representing trials in 25 countries. Our meta-regression confirms that both monetary and non-monetary interventions reduce energy consumption of households, but monetary incentives, of the sizes reported in the literature, tend to show on an average a more pronounced effect. Deploying the right combinations of interventions increases overall effectiveness. We estimate a global carbon emissions reduction potential of 0.35 Gt CO2 yr-1, though deploying the most effective packages of interventions could result in greater reduction. While modest, this potential should be viewed in conjunction with the need for de-risking mitigation pathways with energy demand reductions."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.researchsquare.com/article/rs-124386/v1.pdf?c=1631863515000
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
