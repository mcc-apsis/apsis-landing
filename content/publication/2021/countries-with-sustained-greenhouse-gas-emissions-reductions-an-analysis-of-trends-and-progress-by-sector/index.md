
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Countries with sustained greenhouse gas emissions reductions: an analysis of trends and progress by sector"
authors: 
  - William Lamb
  - M. Grubb
  - Francesca Diluiso
  - Jan Minx
date: 2021-11-26T00:00:00Z
doi: "10.1080/14693062.2021.1990831"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Climate Policy"
publication_short: ""

abstract: "ABSTRACT While global greenhouse gas (GHG) emissions are still rising, a number of countries have emerged with a sustained record of emissions reductions. In this article, we identify these countries and examine their progress, exploring how fast, how deep, and in which sectors they have reduced emissions. We analyse changes in all major GHG emissions sources, with both production – and consumption-based accounting, but exclude very small countries with high volatility, along with land-use, land-use change and forestry CO2 emissions. We find that 24 countries have sustained reductions in annual CO2 and GHG emissions between 1970 and 2018, in total equalling 3.2 GtCO2eq since their respective emissions peaks. In all but three countries, overall GHG reductions are less than energy and industrial CO2 reductions alone. We group countries into three types of emissions pathway: six former Eastern Bloc countries, where emissions declined rapidly in the 1990s and have continued on a downward trajectory since; six Long-term decline countries, which have sustained reductions since the 1970s; and 12 Recent peak countries, whose emissions decline began in the 2000s. In all cases, emissions reductions were achieved primarily in the energy systems sector, specifically in electricity and heat generation, which still remains the largest source of emissions in most countries. By contrast, in the transport sector, emissions tend to be stable or increasing. Transport is the second largest source of current emissions in Recent peak and Long-term decline countries. While the total GHG reductions of these 24 countries are trivial compared to recent global emissions growth, some have achieved a decline of up to 50% in their annual emissions, showing what is possible even under very moderate climate action. Most countries achieved emissions reductions alongside sustained economic growth, and some approached the fast annual rates of change that will be needed across the world in the coming decades to limit warming to 2°C. This raises the hope that more substantive climate policy, as planned in a growing number of countries, may bring about deeper and more rapid emissions reductions than some may expect today. Key policy insights 24 countries have sustained CO2 and GHG emissions reductions between 1970 and 2018 The annual emissions reductions of some countries are within the range of those needed to limit global warming to 2°C, but not consistently, nor across all underlying sectors Most emissions reductions were achieved in the energy sector; transport emissions have remained stable or continue to grow"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
