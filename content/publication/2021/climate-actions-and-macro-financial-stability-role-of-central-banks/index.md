
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Climate actions and macro-financial stability: The role of central banks"
authors: 
  - Francesca Diluiso
  - Barbara Annicchiarico
  - M. Kalkuhl
  - Jan Minx
date: 2021-10-01T00:00:00Z
doi: "10.1016/j.jeem.2021.102548"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Journal of Environmental Economics and Management"
publication_short: ""

abstract: "Limiting global warming to well below 2 °C may pose threats to macroeconomic and financial stability. In an estimated Euro Area New Keynesian model with financial frictions and climate policy, we study the possible perils of a low-carbon transition and evaluate the role of monetary policy and financial regulation. We show that, even for very ambitious climate targets, transition costs are moderate along a timely and gradual mitigation pathway. Inflation volatility strongly increases for disorderly climate policy, demanding a strong monetary response by central banks. In reaction to an adverse financial shock originating in the fossil sector, a green quantitative easing policy can provide an effective stimulus to the economy, but its stabilizing properties do not significantly differ from those of market neutral asset purchase programs. A financial regulation, encouraging the decarbonization of the banks’ balance sheets via ad hoc capital requirements, can significantly reduce the severity of a financial crisis, but prolongs the recovery phase. Our results suggest that the involvement of central banks in climate actions must be carefully designed to be in compliance with their mandate and to avoid unintended trade-offs."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
