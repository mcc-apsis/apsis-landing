
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Progress in climate change adaptation research"
authors: 
  - Anne J Sietsma
  - J. Ford
  - Max Callaghan
  - Jan Minx
date: 2021-04-14T00:00:00Z
doi: "10.1088/1748-9326/abf7f3"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "The scientific literature on climate change adaptation has become too large to assess manually. Beyond standard scientometrics, questions about if and how the field is progressing thus remain largely unanswered. Here we provide a novel, inquisitive, computer-assisted evidence mapping methodology that combines expert interviews (n = 26) and structural topic modelling to evaluate open-ended research questions on progress in the field. We apply this to 62 191 adaptation-relevant scientific publications (1988–2020), selected through supervised machine learning from a comprehensive climate change query. Comparing the literature to key benchmarks of mature adaptation research, our findings align with trends in the adaptation literature observed by most experts: the field is maturing, growing rapidly, and diversifying, with social science and implementation topics arising next to the still-dominant natural sciences and impacts-focused research. Formally assessing the representativeness of IPCC citations, we find evidence of a delay effect for fast-growing areas of research like adaptation strategies and governance. Similarly, we show significant topic biases by geographic location: especially disaster and development-related topics are often studied in Southern countries by authors from the North, while Northern countries dominate governance topics. Moreover, there is a general paucity of research in some highly vulnerable countries. Experts lastly signal a need for meaningful stakeholder involvement. Expanding on the methods presented here would aid the comprehensive and transparent monitoring of adaptation research. For the evidence synthesis community, our methodology provides an example of how to move beyond the descriptive towards the inquisitive and formally evaluating research questions."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
