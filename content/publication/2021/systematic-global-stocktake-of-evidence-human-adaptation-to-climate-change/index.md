
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A systematic global stocktake of evidence on human adaptation to climate change"
authors: 
  - L. Berrang‐Ford
  - A. Siders
  - A. Lesnikowski
  - A. P. Fischer
  - Max Callaghan
  - N. Haddaway
  - K. Mach
  - Malcolm Araos
  - M. Shah
  - Mia Wannewitz
  - D. Doshi
  - T. Leiter
  - C. Matavel
  - J. I. Musah-Surugu
  - G. Wong‐Parodi
  - P. Antwi-Agyei
  - I. Ajibade
  - Neha Chauhan
  - William Kakenmaster
  - C. Grady
  - V. Chalastani
  - K. Jagannathan
  - E. Galappaththi
  - A. Sitati
  - G. Scarpa
  - E. Totin
  - K. Davis
  - N. Hamilton
  - C. Kirchhoff
  - Praveen Kumar
  - B. Pentz
  - N. Simpson
  - Emily Theokritoff
  - D. Deryng
  - D. Reckien
  - C. Zavaleta-Cortijo
  - N. Ulibarri
  - A. Segnon
  - V. Khavhagali
  - Y. Shang
  - Z. Zommers
  - Jiren Xu
  - P. Williams
  - I. V. Canosa
  - Nicole van Maanen
  - Bianca van Bavel
  - M. V. van Aalst
  - Lynée L. Turek-Hankins
  - Hasti Trivedi
  - C. Trisos
  - A. Thomas
  - Shinny Thakur
  - S. Templeman
  - L. Stringer
  - Garry Sotnik
  - K. D. Sjostrom
  - C. Singh
  - Mariella Z. Siña
  - Roopam Shukla
  - J. Sardans
  - E. A. Salubi
  - Lolita Shaila Safaee Chalkasra
  - R. Ruiz-Díaz
  - C. Richards
  - Pratik Pokharel
  - J. Petzold
  - J. Peñuelas
  - Julia Pelaez Avila
  - Julia B. Pazmino Murillo
  - S. Ouni
  - Jennifer Niemann
  - Miriam Nielsen
  - M. New
  - Patricia Nayna Schwerdtle
  - Gabriela Nagle Alverio
  - Cristina A. Mullin
  - Joshua Mullenite
  - A. Mosurska
  - M. Morecroft
  - Jan Minx
  - G. Maskell
  - A. Nunbogu
  - A. Magnan
  - S. Lwasa
  - Megan Lukas-Sithole
  - T. Lissner
  - Oliver Lilford
  - S. Koller
  - Matthew Jurjonas
  - E. Joe
  - L. Huynh
  - A. Hill
  - R. Hernandez
  - G. Hegde
  - Tom Hawxwell
  - S. Harper
  - A. Harden
  - M. Haasnoot
  - E. Gilmore
  - Leah Gichuki
  - A. Gatt
  - Matthias Garschagen
  - J. Ford
  - A. Forbes
  - Aidan Farrell
  - C. Enquist
  - Susan Elliott
  - Emily Duncan
  - Erin Coughlan de Perez
  - S. Coggins
  - Tara Chen
  - D. Campbell
  - K. Browne
  - K. Bowen
  - R. Biesbroek
  - I. Bhatt
  - R. Bezner Kerr
  - S. Barr
  - Emily Baker
  - S. Austin
  - I. Arotoma-Rojas
  - C. Anderson
  - Warda Ajaz
  - Tanvi Agrawal
  - Thelma Zulfawu Abu
date: 2021-10-28T00:00:00Z
doi: "10.1038/s41558-021-01170-y"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "Assessing global progress on human adaptation to climate change is an urgent priority. Although the literature on adaptation to climate change is rapidly expanding, little is known about the actual extent of implementation. We systematically screened >48,000 articles using machine learning methods and a global network of 126 researchers. Our synthesis of the resulting 1,682 articles presents a systematic and comprehensive global stocktake of implemented human adaptation to climate change. Documented adaptations were largely fragmented, local and incremental, with limited evidence of transformational adaptation and negligible evidence of risk reduction outcomes. We identify eight priorities for global adaptation research: assess the effectiveness of adaptation responses, enhance the understanding of limits to adaptation, enable individuals and civil society to adapt, include missing places, scholars and scholarship, understand private sector responses, improve methods for synthesizing different forms of evidence, assess the adaptation at different temperature thresholds, and improve the inclusion of timescale and the dynamics of responses."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://vtechworks.lib.vt.edu/bitstream/10919/108066/2/Lea%20et%20al_EG_Global%20stocktake_NCC_2021.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
