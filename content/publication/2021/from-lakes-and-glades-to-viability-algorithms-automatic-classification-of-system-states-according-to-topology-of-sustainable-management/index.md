
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "From lakes and glades to viability algorithms: Automatic classification of system states according to the Topology of Sustainable Management"
authors: 
  - T. Kittel
  - Finn Müller-Hansen
  - Rebekka Koch
  - J. Heitzig
  - G. Deffuant
  - J. Mathias
  - J. Kurths
date: 2020-12-01T00:00:00Z
doi: "10.1140/epjs/s11734-021-00262-2"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "European Physical Journal"
publication_short: ""

abstract: "The framework Topology of Sustainable Management by Heitzig et al. (2016) distinguishes qualitatively different regions in state space of dynamical models representing manageable systems with default dynamics. In this paper, we connect the framework to viability theory by defining its main components based on viability kernels and capture basins. This enables us to use the Saint-Pierre algorithm to visualize the shape and calculate the volume of the main partition of the Topology of Sustainable Management. We present an extension of the algorithm to compute implicitly defined capture basins. To demonstrate the applicability of our approach, we introduce a low-complexity model coupling environmental and socioeconomic dynamics. With this example, we also address two common estimation problems: an unbounded state space and highly varying time scales. We show that appropriate coordinate transformations can solve these problems. It is thus demonstrated how algorithmic approaches from viability theory can be used to get a better understanding of the state space of manageable dynamical systems."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://arxiv.org/pdf/1706.04542
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
