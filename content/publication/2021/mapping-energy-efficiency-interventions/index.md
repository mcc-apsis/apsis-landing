
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Mapping energy efficiency interventions"
authors: 
  - Miriam Berretta
  - C. Zamawe
  - P. Ferraro
  - N. Haddaway
  - Jan Minx
  - Birte Snilstveit
  - J. Eyers
date: 2021-04-01T00:00:00Z
doi: "10.23846/EGM017"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Energy Efficiency EGM"
publication_short: ""

abstract: "Climate change is one of the most challenge of our era, mitigating its effect is one way to fight it. The adoption of energy efficiency (EE) solutions has a great potential to reduce energy demand and consumption, thereby contributing to mitigate the reduction of global CO2 emissions. Over the last decade, there has been an increasing global interest in EE. In 2019 alone, USD250 billion were invested in EE, with numerous programmes, policies and projects implemented to foster the adoption of EE technologies and practices among different end users. By searching both academic and grey literature, we have mapped a total of 283 impact evaluations (IEs) and 16 systematic reviews (SRs) related to the effects of energy efficiency (EE) interventions at the household, public institutions, and business level around the world. The EGM reveals clusters of evidence and key gaps where limited or no evidence exists and more research is needed."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://3ieimpact.org/sites/default/files/2021-01/EGM16-GIZ-FSN.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
