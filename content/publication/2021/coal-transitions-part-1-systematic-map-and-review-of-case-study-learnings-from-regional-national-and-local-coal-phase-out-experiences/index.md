
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Coal transitions—part 1: a systematic map and review of case study learnings from regional, national, and local coal phase-out experiences"
authors: 
  - Francesca Diluiso
  - Paula Walk
  - Niccolò Manych
  - Nicola Cerutti
  - Vladislav Chipiga
  - A. Workman
  - C. Ayas
  - R. Cui
  - Diyang Cui
  - Kaihui Song
  - Lucy A Banisch
  - Nikolaj Moretti
  - Max Callaghan
  - L. Clarke
  - F. Creutzig
  - Jérôme Hilaire
  - F. Jotzo
  - M. Kalkuhl
  - William Lamb
  - A. Löschel
  - Finn Müller-Hansen
  - G. Nemet
  - P. oei
  - B. Sovacool
  - J. Steckel
  - Sebastian Thomas
  - J. Wiseman
  - Jan Minx
date: 2021-10-21T00:00:00Z
doi: "10.1088/1748-9326/ac1b58"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "A rapid coal phase-out is needed to meet the goals of the Paris Agreement, but is hindered by serious challenges ranging from vested interests to the risks of social disruption. To understand how to organize a global coal phase-out, it is crucial to go beyond cost-effective climate mitigation scenarios and learn from the experience of previous coal transitions. Despite the relevance of the topic, evidence remains fragmented throughout different research fields, and not easily accessible. To address this gap, this paper provides a systematic map and comprehensive review of the literature on historical coal transitions. We use computer-assisted systematic mapping and review methods to chart and evaluate the available evidence on historical declines in coal production and consumption. We extracted a dataset of 278 case studies from 194 publications, covering coal transitions in 44 countries and ranging from the end of the 19th century until 2021. We find a relatively recent and rapidly expanding body of literature reflecting the growing importance of an early coal phase-out in scientific and political debates. Previous evidence has primarily focused on the United Kingdom, the United States, and Germany, while other countries that experienced large coal declines, like those in Eastern Europe, are strongly underrepresented. An increasing number of studies, mostly published in the last 5 years, has been focusing on China. Most of the countries successfully reducing coal dependency have undergone both demand-side and supply-side transitions. This supports the use of policy approaches targeting both demand and supply to achieve a complete coal phase-out. From a political economy perspective, our dataset highlights that most transitions are driven by rising production costs for coal, falling prices for alternative energies, or local environmental concerns, especially regarding air pollution. The main challenges for coal-dependent regions are structural change transformations, in particular for industry and labor. Rising unemployment is the most largely documented outcome in the sample. Policymakers at multiple levels are instrumental in facilitating coal transitions. They rely mainly on regulatory instruments to foster the transitions and compensation schemes or investment plans to deal with their transformative processes. Even though many models suggest that coal phase-outs are among the low-hanging fruits on the way to climate neutrality and meeting the international climate goals, our case studies analysis highlights the intricate political economy at work that needs to be addressed through well-designed and just policies."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
