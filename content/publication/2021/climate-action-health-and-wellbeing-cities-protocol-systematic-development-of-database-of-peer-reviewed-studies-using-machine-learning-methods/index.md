
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Climate action for health and wellbeing in cities: a protocol for the systematic development of a database of peer-reviewed studies using machine learning methods"
authors: 
  - K. Belesova
  - Max Callaghan
  - Jan Minx
  - F. Creutzig
  - C. Turcu
  - E. Hutchinson
  - J. Milner
  - M. Crane
  - A. Haines
  - M. Davies
  - P. Wilkinson
date: 2021-03-05T00:00:00Z
doi: "10.12688/wellcomeopenres.16570.1"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2", "2"]

# Publication name and optional abbreviated publication name.
publication: "Wellcome Open Research"
publication_short: ""

abstract: "Cities produce more than 70% of global greenhouse gas emissions. Action by cities is therefore crucial for climate change mitigation as well as for safeguarding the health and wellbeing of their populations under climate change. Many city governments have made ambitious commitments to climate change mitigation and adaptation and implemented a range of actions to address them. However, a systematic record and synthesis of the findings of evaluations of the effect of such actions on human health and wellbeing is currently lacking. This, in turn, impedes the development of robust knowledge on what constitutes high-impact climate actions of benefit to human health and wellbeing, which can inform future action plans, their implementation and scale-up. The development of a systematic record of studies reporting climate and health actions in cities is made challenging by the broad landscape of relevant literature scattered across many disciplines and sectors, which is challenging to effectively consolidate using traditional literature review methods. This protocol reports an innovative approach for the systematic development of a database of studies of climate change mitigation and adaptation actions implemented in cities, and their benefits (or disbenefits) for human health and wellbeing, derived from peer-reviewed academic literature. Our approach draws on extensive tailored search strategies and machine learning methods for article classification and tagging to generate a database for subsequent systematic reviews addressing questions of importance to urban decision-makers on climate actions in cities for human health and wellbeing."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
