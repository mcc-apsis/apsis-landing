
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Decision makers need 'living' evidence synthesis"
authors: 
  - Julian H. Elliott
  - R. Lawrence
  - Jan Minx
  - O. Oladapo
  - P. Ravaud
  - Britta Tendal Jeppesen
  - James Thomas
  - T. Turner
  - P. Vandvik
  - J. Grimshaw
date: 2021-12-01T00:00:00Z
doi: "10.1038/d41586-021-03690-1"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4", "2"]

# Publication name and optional abbreviated publication name.
publication: "Nature"
publication_short: ""

abstract: "Fund and use dynamic evidence summaries of the latest data to steer research, practice and policy."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.nature.com/articles/d41586-021-03690-1.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
