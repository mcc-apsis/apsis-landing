
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The Global Adaption Mapping Initiative (GAMI): Part 3 - Coding protocol"
authors: 
  - A. Lesnikowski
  - L. Berrang‐Ford
  - A. Siders
  - N. Haddaway
  - R. Biesbroek
  - S. Harper
  - Jan Minx
  - E. Perez
  - D. Reckien
  - M. New
  - C. Singh
  - A. Thomas
  - E. Totin
  - C. Trisos
  - Bianca van Bavel
date: 2021-02-05T00:00:00Z
doi: "10.21203/rs.3.pex-1242/v1"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "PROTOCOL"
publication_short: ""

abstract: "
 Context: It is now widely accepted that the climate is changing, and that societal response will need to be rapid and comprehensive to prevent the most severe impacts. A key milestone in global climate governance is to assess progress on adaptation. To-date, however, there has been negligible robust, systematic synthesis of progress on adaptation or adaptation-relevant responses globally. Aim: The purpose of this review protocol is to outline the methods used by the Global Adaptation Mapping Initiative (GAMI) to systematically review human adaptation responses to climate-related changes that have been documented globally since 2013 in the scientific literature. The broad question underpinning this review is: Are we adapting to climate change? More specifically, we ask ‘what is the evidence relating to human adaptation-related responses that can (or are) directly reducing risk, exposure, and/or vulnerability to climate change?’ Methods: We review scientific literature 2013-2019 to identify documents empirically reporting on observed adaptation-related responses to climate change in human systems that can directly reduce risk. We exclude non-empirical (theoretical & conceptual) literature and adaptation in natural systems that occurs without human intervention. Included documents were coded across a set of questions focused on: Who is responding? What responses are documented? What is the extent of the adaptation-related response? What is the evidence that adaptation-related responses reduce risk, exposure and/or vulnerability? Once articles are coded, we conduct a quality appraisal of the coding and develop ‘evidence packages’ for regions and sectors. We supplement this systematic mapping with an expert elicitation exercise, undertaken to assess bias and validity of insights from included/coded literature vis a vis perceptions of real-world adaptation for global regions and sectors, with associated confidence assessments. Related protocols: This protocol represents Part 3 of a 5-part series outlining the phases of this initiative. Part 3 outlines the methods used to extract data on adaptation from documents (coding), as well as procedures for data quality assurance. See Figure 1."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.researchsquare.com/article/pex-1242/latest.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
