
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A comprehensive and synthetic dataset for global, regional, and national greenhouse gas emissions by sector 1970–2018 with an extension to 2019"
authors: 
  - Jan Minx
  - William Lamb
  - R. Andrew
  - J. Canadell
  - M. Crippa
  - Niklas Döbbeling
  - P. Forster
  - D. Guizzardi
  - J. Olivier
  - G. Peters
  - J. Pongratz
  - A. Reisinger
  - M. Rigby
  - M. Saunois
  - Steven J. Smith
  - E. Solazzo
  - H. Tian
date: 2021-11-10T00:00:00Z
doi: "10.5194/essd-13-5213-2021"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Earth System Science Data"
publication_short: ""

abstract: "Abstract. To track progress towards keeping global warming well below 2 ∘C or even 1.5 ∘C, as agreed in the Paris Agreement, comprehensive
up-to-date and reliable information on anthropogenic emissions and removals
of greenhouse gas (GHG) emissions is required. Here we compile a new synthetic dataset on anthropogenic GHG emissions for 1970–2018 with a
fast-track extension to 2019. Our dataset is global in coverage and includes
CO2 emissions, CH4 emissions, N2O emissions, as well as those from fluorinated gases (F-gases: HFCs, PFCs, SF6, NF3) and
provides country and sector details. We build this dataset from the version 6 release of the Emissions Database for Global Atmospheric Research (EDGAR v6) and three bookkeeping models for CO2 emissions from land use,
land-use change, and forestry (LULUCF). We assess the uncertainties of global greenhouse gases at the 90 % confidence interval (5th–95th
percentile range) by combining statistical analysis and comparisons of
global emissions inventories and top-down atmospheric measurements with an
expert judgement informed by the relevant scientific literature. We identify
important data gaps for F-gas emissions. The agreement between our bottom-up inventory estimates and top-down
atmospheric-based emissions estimates is relatively close for some F-gas
species (∼ 10 % or less), but estimates can differ by an order of magnitude or more for others. Our aggregated F-gas estimate is about 10 % lower than top-down estimates in recent years. However, emissions from excluded F-gas species such as
chlorofluorocarbons (CFCs) or hydrochlorofluorocarbons (HCFCs) are
cumulatively larger than the sum of the reported species. Using global
warming potential values with a 100-year time horizon from the Sixth Assessment Report by the Intergovernmental Panel on Climate Change (IPCC),
global GHG emissions in 2018 amounted to 58 ± 6.1 GtCO2 eq.
consisting of CO2 from fossil fuel combustion and industry (FFI) 38 ± 3.0 GtCO2, CO2-LULUCF 5.7 ± 4.0 GtCO2, CH4 10 ± 3.1 GtCO2 eq., N2O
2.6 ± 1.6 GtCO2 eq., and F-gases 1.3 ± 0.40 GtCO2 eq. Initial estimates suggest further growth of 1.3 GtCO2 eq. in GHG emissions to reach 59 ± 6.6 GtCO2 eq. by 2019. Our analysis of
global trends in anthropogenic GHG emissions over the past 5 decades (1970–2018) highlights a pattern of varied but sustained emissions growth. There is high confidence that global anthropogenic GHG emissions have
increased every decade, and emissions growth has been persistent across the different (groups of) gases. There is also high confidence that global
anthropogenic GHG emissions levels were higher in 2009–2018 than in any previous decade and that GHG emissions levels grew throughout the most recent decade. While the average annual GHG emissions growth rate slowed between
2009 and 2018 (1.2 % yr−1) compared to 2000–2009 (2.4 % yr−1), the absolute increase in average annual GHG emissions by decade was never
larger than between 2000–2009 and 2009–2018. Our analysis further reveals
that there are no global sectors that show sustained reductions in GHG
emissions. There are a number of countries that have reduced GHG emissions
over the past decade, but these reductions are comparatively modest and
outgrown by much larger emissions growth in some developing countries such
as China, India, and Indonesia. There is a need to further develop independent, robust, and timely emissions estimates across all gases. As such, tracking progress in climate policy requires substantial investments
in independent GHG emissions accounting and monitoring as well as in national and international statistical infrastructures. The data associated
with this article (Minx et al., 2021) can be found at https://doi.org/10.5281/zenodo.5566761.
"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://essd.copernicus.org/articles/13/5213/2021/essd-13-5213-2021.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
