
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The effects on public health of climate change adaptation responses: a systematic review of evidence from low- and middle-income countries"
authors: 
  - P. Scheelbeek
  - A. Dangour
  - Stephanie Jarmul
  - Grace Turner
  - Anne J Sietsma
  - Jan Minx
  - Max Callaghan
  - I. Ajibade
  - S. Austin
  - R. Biesbroek
  - K. Bowen
  - Tara Chen
  - K. Davis
  - T. Ensor
  - J. Ford
  - E. Galappaththi
  - E. Joe
  - I. J. Musah-Surugu
  - G. N. Alverio
  - P. Schwerdtle
  - Pratik Pokharel
  - E. A. Salubi
  - G. Scarpa
  - A. Segnon
  - Mariella Z. Siña
  - S. Templeman
  - Jiren Xu
  - C. Zavaleta-Cortijo
  - L. Berrang‐Ford
date: 2021-07-01T00:00:00Z
doi: "10.1088/1748-9326/ac092c"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2", "2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "Climate change adaptation responses are being developed and delivered in many parts of the world in the absence of detailed knowledge of their effects on public health. Here we present the results of a systematic review of peer-reviewed literature reporting the effects on health of climate change adaptation responses in low- and middle-income countries (LMICs). The review used the ‘Global Adaptation Mapping Initiative’ database (comprising 1682 publications related to climate change adaptation responses) that was constructed through systematic literature searches in Scopus, Web of Science and Google Scholar (2013–2020). For this study, further screening was performed to identify studies from LMICs reporting the effects on human health of climate change adaptation responses. Studies were categorised by study design and data were extracted on geographic region, population under investigation, type of adaptation response and reported health effects. The review identified 99 studies (1117 reported outcomes), reporting evidence from 66 LMICs. Only two studies were ex ante formal evaluations of climate change adaptation responses. Papers reported adaptation responses related to flooding, rainfall, drought and extreme heat, predominantly through behaviour change, and infrastructural and technological improvements. Reported (direct and intermediate) health outcomes included reduction in infectious disease incidence, improved access to water/sanitation and improved food security. All-cause mortality was rarely reported, and no papers were identified reporting on maternal and child health. Reported maladaptations were predominantly related to widening of inequalities and unforeseen co-harms. Reporting and publication-bias seems likely with only 3.5% of all 1117 health outcomes reported to be negative. Our review identified some evidence that climate change adaptation responses may have benefits for human health but the overall paucity of evidence is concerning and represents a major missed opportunity for learning. There is an urgent need for greater focus on the funding, design, evaluation and standardised reporting of the effects on health of climate change adaptation responses to enable evidence-based policy action."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://iopscience.iop.org/article/10.1088/1748-9326/ac092c/pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
