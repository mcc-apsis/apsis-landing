
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Face masks increase compliance with physical distancing recommendations during the COVID‑19 pandemic"
authors: 
  - Gyula Seres
  - Anna Balleyer
  - Nicola Cerutti
  - A. Danilov
  - J. Friedrichsen
  - Yiming Liu
  - Müge Süer
date: 2020-05-23T00:00:00Z
doi: "10.1007/s40881-021-00108-6"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Journal of the Economic Science Association"
publication_short: ""

abstract: "Governments across the world have implemented restrictive policies to slow the spread of COVID-19. Recommended face mask use has been a controversially discussed policy, among others, due to potential adverse effects on physical distancing. Using a randomized field experiment (N = 300), we show that individuals kept a significantly larger distance from someone wearing a face mask than from an unmasked person during the early days of the pandemic. According to an additional survey experiment (N = 456) conducted at the time, masked individuals were not perceived as being more infectious than unmasked ones, but they were believed to prefer more distancing. This result suggests that wearing a mask served as a social signal that led others to increase the distance they kept. Our findings provide evidence against the claim that mask use creates a false sense of security that would negatively affect physical distancing. Furthermore, our results suggest that behavior has informational content that may be affected by policies."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://link.springer.com/content/pdf/10.1007/s40881-021-00108-6.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
