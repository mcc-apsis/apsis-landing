
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The political economy of national climate policy: Architectures of constraint and a typology of countries"
authors: 
  - William Lamb
  - Jan Minx
date: 2020-06-01T00:00:00Z
doi: "10.1016/j.erss.2020.101429"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Energy Research & Social Science"
publication_short: ""

abstract: " In the wake of the Paris Agreement, countries have yet to embark on deep  decarbonisation pathways. This article explores the reasons for this  limited response, taking a comparative political economy lens to  identify national constraints that actively hinder climate policy  progress. We discuss different metrics of climate policy progress,  including emissions trends, climate legislation adoption, policy  adoption, policy stringency, and policy outcomes. We then review  literatures that explain varying national outcomes along these  dimensions. Identified constraints include (but are not limited to)  exposure to fossil fuel extraction activities, supply-side coal  dependency, a lack of democratic norms, exposure to corruption, a lack  of public climate awareness, and low levels of social trust. Correlation  and principal component analysis of these variables demonstrates strong  co-dependencies, including a North-South divide in institutional  quality, trust and climate awareness that limits full participation in  climate legislation and the removal of fossil subsidies. Recent trends  indicate stability in corruption across the whole sample, and the  continued durability of autocratic and extractivist states. We identify  common constraints for five distinct country groups using cluster  analysis: ‘oil & gas states’, ‘fragile states’, ‘coal-dependent  development’, ‘fractured democracies’ and ‘wealthy OECD’. We highlight  the need to scrutinise architectures of constraint – combinations of  political economic factors that are mutually reinforcing and highly  resistant to intervention."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
