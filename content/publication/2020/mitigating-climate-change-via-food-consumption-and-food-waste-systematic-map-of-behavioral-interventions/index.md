
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Mitigating climate change via food consumption and food waste: A systematic map of behavioral interventions"
authors: 
  - L. Reisch
  - C. Sunstein
  - M. Andor
  - F. Doebbe
  - Johanna Meier
  - Neal R Haddaway
  - Neal R Haddaway
date: 2020-08-10T00:00:00Z
doi: "10.1016/j.jclepro.2020.123717"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Journal of Cleaner Production"
publication_short: ""

abstract: "Demand-side policies for mitigating climate change based on behavioral insights are gaining increased attention in research and practice. Here we describe a systematic map that catalogues existing research on behaviorally informed interventions targeting changes in consumer food consumption and food waste behavior. The purpose is to gain an overview of research foci and gaps, providing an evidence base for deeper analysis. In terms of food consumption, we focus on animal protein (meat, fish, dairy, and eggs) and its substitutes. The map follows the standards for evidence synthesis from the Collaboration for Environmental Evidence (CEE) as well as the RepOrting Standards for Systematic Evidence Syntheses (ROSES). We identified 49 articles including 56 separate studies, as well as 18 literature reviews. We find a variety of study designs with a focus on canteen and restaurant studies as well as a steep increase of publications since 2016. We create an interactive evidence atlas that plots these studies across geographical space. Here, we find a concentration of research in the Anglo-Saxon world. Most studies follow multi-intervention designs and focus on actual food consumption behavior, fewer on food waste behavior. We identify knowledge clusters amenable for a systematic review focusing on the effectiveness of these interventions, namely: priming, disclosure, defaults, social norms, micro-environment changes, and ease of use. The systematic map highlights knowledge gaps, where more primary research is needed and evidence cannot support policy; it identifies knowledge clusters, where sufficient studies exist but there is a lack of clarity over effectiveness, and so full synthesis can be conducted rapidly; finally, it reveals patterns in research methods that can highlight best practices and issues with methodology that can support the improvement of primary evidence production and mitigation of research waste. To the best of our knowledge, this is the first systematic study mapping this specific area."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
