
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Quantifying the potential for climate change mitigation of consumption options"
authors: 
  - D. Ivanova
  - J. Barrett
  - Dominik Wiedenhofer
  - Biljana Macura
  - Max Callaghan
  - F. Creutzig
date: 2020-04-01T00:00:00Z
doi: "10.1088/1748-9326/ab8589"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "Background. Around two-thirds of global GHG emissions are directly and indirectly linked to household consumption, with a global average of about 6 tCO2eq/cap. The average per capita carbon footprint of North America and Europe amount to 13.4 and 7.5 tCO2eq/cap, respectively, while that of Africa and the Middle East—to 1.7 tCO2eq/cap on average. Changes in consumption patterns to low-carbon alternatives therefore present a great and urgently required potential for emission reductions. In this paper, we synthesize emission mitigation potentials across the consumption domains of food, housing, transport and other consumption. Methods. We systematically screened 6990 records in the Web of Science Core Collections and Scopus. Searches were restricted to (1) reviews of lifecycle assessment studies and (2) multiregional input-output studies of household consumption, published after 2011 in English. We selected against pre-determined eligibility criteria and quantitatively synthesized findings from 53 studies in a meta-review. We identified 771 original options, which we summarized and presented in 61 consumption options with a positive mitigation potential. We used a fixed-effects model to explore the role of contextual factors (geographical, technical and socio-demographic factors) for the outcome variable (mitigation potential per capita) within consumption options. Results and discussion. We establish consumption options with a high mitigation potential measured in tons of CO2eq/capita/yr. For transport, the options with the highest mitigation potential include living car-free, shifting to a battery electric vehicle, and reducing flying by a long return flight with a median reduction potential of more than 1.7 tCO2eq/cap. In the context of food, the highest carbon savings come from dietary changes, particularly an adoption of vegan diet with an average and median mitigation potential of 0.9 and 0.8 tCO2eq/cap, respectively. Shifting to renewable electricity and refurbishment and renovation are the options with the highest mitigation potential in the housing domain, with medians at 1.6 and 0.9 tCO2eq/cap, respectively. We find that the top ten consumption options together yield an average mitigation potential of 9.2 tCO2eq/cap, indicating substantial contributions towards achieving the 1.5 °C–2 °C target, particularly in high-income context."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
