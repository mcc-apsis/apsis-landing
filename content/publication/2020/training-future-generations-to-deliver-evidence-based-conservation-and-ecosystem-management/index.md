
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Training future generations to deliver evidence-based conservation and ecosystem management"
authors: 
  - H. Downey
  - Tatsuya Amano
  - M. Cadotte
  - C. Cook
  - S. Cooke
  - Neal R Haddaway
  - Julia P. G. Jones
  - N. Littlewood
  - J. Walsh
  - M. Abrahams
  - Gilbert B. Adum
  - M. Akasaka
  - J. A. Alves
  - R. Antwis
  - E. Arellano
  - J. Axmacher
  - H. Barclay
  - L. Batty
  - A. Benítez‐López
  - J. Bennett
  - M. Berg
  - S. Bertolino
  - D. Biggs
  - Friederike C. Bolam
  - T. C. Bray
  - B. Brook
  - J. Bull
  - Z. Buřivalová
  - M. Cabeza
  - A. Chauvenet
  - Alec P. Christie
  - L. Cole
  - Alison J. Cotton
  - S. Cotton
  - S. Cousins
  - D. Craven
  - W. Cresswell
  - J. Cusack
  - S. Dalrymple
  - Z. Davies
  - A. Diaz
  - J. Dodd
  - A. Felton
  - E. Fleishman
  - C. Gardner
  - R. Garside
  - A. Ghoddousi
  - J. Gilroy
  - D. Gill
  - J. Gill
  - Louise Glew
  - M. Grainger
  - Amelia A. Grass
  - S. Greshon
  - J. Gundry
  - T. Hart
  - Charlotte R Hopkins
  - C. Howe
  - Arlyne Johnson
  - Kelly W. Jones
  - N. Jordan
  - Taku Kadoya
  - Daphne Kerhoas
  - J. Koricheva
  - T. Lee
  - S. Lengyel
  - Stuart W. Livingstone
  - A. Lyons
  - G. Mccabe
  - J. Millett
  - Chloë M. J. Strevens
  - A. Moolna
  - H. Mossman
  - Nibedita Mukherjee
  - Andrés Muñoz-Sáez
  - N. Negrões
  - Olivia Norfolk
  - T. Osawa
  - S. Papworth
  - Kirsty J. Park
  - J. Pellet
  - A. D. Phillott
  - Joshua M. Plotnik
  - D. Priatna
  - Alejandra G. Ramos
  - N. Randall
  - Rob M. Richards
  - E. Ritchie
  - D. Roberts
  - R. Rocha
  - J. Rodríguez
  - R. Sanderson
  - Takehiro Sasaki
  - S. Savilaakso
  - C. Sayer
  - Ç. Şekercioğlu
  - M. Senzaki
  - Grania Smith
  - Robert J. Smith
  - M. Soga
  - C. Soulsbury
  - M. Steer
  - G. Stewart
  - E. Strange
  - Andrew J. Suggitt
  - Ralph R. J. Thompson
  - Stewart Thompson
  - I. Thornhill
  - R. Trevelyan
  - Hope O. Usieta
  - O. Venter
  - Amanda D. Webber
  - Rachel L. White
  - M. Whittingham
  - A. Wilby
  - R. Yarnell
  - Veronica Zamora
  - W. Sutherland
date: 2020-06-15T00:00:00Z
doi: "10.1002/2688-8319.12032"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Ecological Solutions and Evidence"
publication_short: ""

abstract: "1. To be effective, the next generation of conservation practitioners and managers need to be critical thinkers with a deep understanding of how to make evidence‐based decisions and of the value of evidence synthesis. 
 
2. If, as educators, we do not make these priorities a core part of what we teach, we are failing to prepare our students to make an effective contribution to conservation practice. 
 
3. To help overcome this problem we have created open access online teaching materials in multiple languages that are stored in Applied Ecology Resources. So far, 117 educators from 23 countries have acknowledged the importance of this and are already teaching or about to teach skills in appraising or using evidence in conservation decision‐making. This includes 145 undergraduate, postgraduate or professional development courses. 
 
4. We call for wider teaching of the tools and skills that facilitate evidence‐based conservation and also suggest that providing online teaching materials in multiple languages could be beneficial for improving global understanding of other subject areas."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://dro.deakin.edu.au/articles/journal_contribution/Training_future_generations_to_deliver_evidence_based_conservation_and_ecosystem_management/20674026/1/files/36883764.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
