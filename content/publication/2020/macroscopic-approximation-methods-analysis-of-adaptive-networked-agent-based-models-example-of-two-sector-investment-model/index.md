
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Macroscopic approximation methods for the analysis of adaptive networked agent-based models: Example of a two-sector investment model"
authors: 
  - Jakob J. Kolb
  - Finn Müller-Hansen
  - J. Kurths
  - J. Heitzig
date: 2019-09-30T00:00:00Z
doi: "10.1103/PHYSREVE.102.042311"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Physical Review E"
publication_short: ""

abstract: "In this paper, we propose a statistical aggregation method for agent-based models with heterogeneous agents that interact both locally on a complex adaptive network and globally on a market. The method combines three approaches from statistical physics: (a) moment closure, (b) pair approximation of adaptive network processes, and (c) thermodynamic limit of the resulting stochastic process. As an example of use, we develop a stochastic agent-based model with heterogeneous households that invest in either a fossil-fuel- or renewables-based sector while allocating labor on a competitive market. Using the adaptive voter model, the model describes agents as social learners that interact on a dynamic network. We apply the approximation methods to derive a set of ordinary differential equations that approximate the macrodynamics of the model. A comparison of the reduced analytical model with numerical simulations shows that the approximation fits well for a wide range of parameters. The method makes it possible to use analytical tools to better understand the dynamical properties of models with heterogeneous agents on adaptive networks. We showcase this with a bifurcation analysis that identifies parameter ranges with multistabilities. The method can thus help to explain emergent phenomena from network interactions and make them mathematically traceable."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://arxiv.org/pdf/1909.13758
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
