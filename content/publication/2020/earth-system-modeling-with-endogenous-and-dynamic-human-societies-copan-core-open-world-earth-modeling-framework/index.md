
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Earth system modeling with endogenous and dynamic human societies: the copan:CORE open World–Earth modeling framework"
authors: 
  - J. Donges
  - J. Heitzig
  - W. Barfuss
  - M. Wiedermann
  - Johannes A. Kassel
  - T. Kittel
  - Jakob J. Kolb
  - Till Kolster
  - Finn Müller-Hansen
  - I. Otto
  - Kilian B. Zimmerer
  - W. Lucht
date: 2019-09-30T00:00:00Z
doi: "10.5194/esd-11-395-2020"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Earth System Dynamics"
publication_short: ""

abstract: "Analysis of Earth system dynamics in the Anthropocene requires to explicitly take into account the increasing magnitude of processes operating in human societies, their cultures, economies and technosphere and their growing feedback entanglement with those in the physical, chemical and biological systems of the planet. However, current state-of-the-art Earth System Models do not represent dynamic human societies and their feedback interactions with the biogeophysical Earth system and macroeconomic Integrated Assessment Models typically do so only with limited scope. This paper (i) proposes design principles for constructing World-Earth Models (WEM) for Earth system analysis of the Anthropocene, i.e., models of social (World) - ecological (Earth) co-evolution on up to planetary scales, and (ii) presents the copan:CORE open simulation modeling framework for developing, composing and analyzing such WEMs based on the proposed principles. The framework provides a modular structure to flexibly construct and study WEMs. These can contain biophysical (e.g. carbon cycle dynamics), socio-metabolic/economic (e.g. economic growth) and socio-cultural processes (e.g. voting on climate policies or changing social norms) and their feedback interactions, and are based on elementary entity types, e.g., grid cells and social systems. Thereby, copan:CORE enables the epistemic flexibility needed for contributions towards Earth system analysis of the Anthropocene given the large diversity of competing theories and methodologies used for describing socio-metabolic/economic and socio-cultural processes in the Earth system by various fields and schools of thought. To illustrate the capabilities of the framework, we present an exemplary and highly stylized WEM implemented in copan:CORE that illustrates how endogenizing socio-cultural processes and feedbacks could fundamentally change macroscopic model outcomes."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://esd.copernicus.org/articles/11/395/2020/esd-11-395-2020.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
