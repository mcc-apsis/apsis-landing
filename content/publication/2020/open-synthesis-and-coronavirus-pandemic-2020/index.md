
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Open synthesis and the coronavirus pandemic in 2020"
authors: 
  - Neal R Haddaway
  - E. Akl
  - M. Page
  - V. Welch
  - Ciara Keenan
  - T. Lotfi
date: 2020-07-01T00:00:00Z
doi: "10.1016/j.jclinepi.2020.06.032"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Journal of Clinical Epidemiology"
publication_short: ""

abstract: "The COVID-19 pandemic of 2020 has caused high levels of mortality and continues to threaten the lives of the global population. The pandemic has amounted to a “once in a lifetime” event for humanity and has affected it across its different sectors of existence: health, education, economy and environment, among others. The pandemic continues to threaten job prospects for millions of people and has resulted in widespread economic turmoil. It has also led to the cancellation of numerous conferences and research fieldwork and to closed offices across the globe."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://europepmc.org/articles/pmc7328560?pdf=render
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
