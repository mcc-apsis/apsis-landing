
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Four agendas for research and policy on emissions mitigation and well-being"
authors: 
  - J. Roberts
  - J. Steinberger
  - Thomas Dietz
  - William Lamb
  - Richard York
  - Andrew K. Jorgenson
  - Jennifer E. Givens
  - P. Baer
  - J. Schor
date: 2020-01-13T00:00:00Z
doi: "10.1017/sus.2019.25"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Global Sustainability"
publication_short: ""

abstract: "Non-technical abstract The climate crisis requires nations to achieve human well-being with low national levels of carbon emissions. Countries vary from one another dramatically in how effectively they convert resources into well-being, and some nations with low levels of emissions have relatively high objective and subjective well-being. We identify urgent research and policy agendas for four groups of countries with either low or high emissions and well-being indicators. Least studied are those with low well-being and high emissions. Understanding social and political barriers to switching from high-carbon to lower-carbon modes of production and consumption, and ways to overcome them, will be fundamental. Technical abstract In order for rapid decarbonization of the world's economies to be consistent with staying within 1.5 (or even 2) °C of warming limits, research is urgently needed on the potential for decoupling human well-being improvements from energy use and greenhouse gas emissions. Improving human well-being is both a higher moral priority, and a more promising candidate for decoupling, than economic growth. Research needs to include the difficult political-economic, judicial and institutional changes needed to support transitions to high well-being and low-carbon pathways. Much of this work will be nation-specific, but it is useful to examine pathways for four groups of nations. We propose an initial set of research questions for each group of nations, on their history and current situation, and on pathways to rapid decarbonization. Existing technology now makes it feasible to achieve low carbon emissions and high human well-being for all nations. But the barriers are substantial, and include addressing existing vested interests of economic sectors, technological lock-in, assumptions embedded in culture, and political structures. Unfortunately, these areas are currently the weakest link in the existing research and policy chain."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.cambridge.org/core/services/aop-cambridge-core/content/view/FF21DE61152F97ACA39B041E1E016CA7/S2059479819000255a.pdf/div-class-title-four-agendas-for-research-and-policy-on-emissions-mitigation-and-well-being-div.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
