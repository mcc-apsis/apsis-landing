
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A topography of climate change research"
authors: 
  - Max Callaghan
  - Jan Minx
  - P. Forster
date: 2020-01-27T00:00:00Z
doi: "10.1038/s41558-019-0684-5"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "The massive expansion of scientific literature on climate change poses challenges for global environmental assessments and our understanding of how these assessments work. Big data and machine learning can help us deal with large collections of scientific text, making the production of assessments more tractable, and giving us better insights about how past assessments have engaged with the literature. We use topic modelling to draw a topic map, or topography, of over 400,000 publications from the Web of Science on climate change. We update current knowledge on the IPCC, showing that compared with the baseline of the literature identified, the social sciences are in fact over-represented in recent assessment reports. Technical, solutions-relevant knowledge—especially in agriculture and engineering—is under-represented. We suggest a variety of other applications of such maps, and our findings have direct implications for addressing growing demands for more solution-oriented climate change assessments that are also more firmly rooted in the social sciences2,3. The perceived lack of social science knowledge in assessment reports does not necessarily imply an IPCC bias, but rather suggests a need for more social science research with a focus on technical topics on climate solutions."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://eprints.whiterose.ac.uk/159983/1/main.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
