
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Editorial: Fifty Campbell systematic reviews relevant to the policy response to COVID‐19"
authors: 
  - Campbell Editorial Board
date: 2020-08-10T00:00:00Z
doi: "10.1002/cl2.1107"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2", "2"]

# Publication name and optional abbreviated publication name.
publication: "Campbell Systematic Reviews"
publication_short: ""

abstract: "The global severe acute respiratory syndrome coronavirus 2 pandemic strikingly shows the need for rigorous evidence to inform decisions. During such times of crisis, many decisions are made across multiple sectors and trillions of dollars are spent to deal with its consequences that affect all aspects of economic and societal life. Given the scale of human suffering, thoughtfully designing effective policies, and carefully spending scarce resources on interventions that work during crisis management and recovery, become crucial. However, in many areas of decision making, the use of robust and reliable evidence is not the norm. This has dire consequences: evidence from impact evaluations in different sectors show that about 80% of policy interventions are not effective (White, 2019). Equally, the reliance on an individual study or model rather than evidence synthesis commonly leads to misinformed policy and outright harm. For example, the retracted study on hydroxychloroquine for COVID‐19 led to public harm as well as public mistrust (Mehra, Ruschitzka, & Patel, 2020). Now, more than ever, public policy needs to be informed by the most rigorous, comprehensive and up‐to‐date evidence possible. We, at the Campbell Collaboration, are working on both providing this rigorous evidence and promoting its use to inform decisions about social and public policy. Campbell systematic reviews provide a wealth of rigorous evidence to support social and economic response. These reviews highlight what is known and actionable, and point to critical questions decisionmakers need to ask in planning and implementing social and economic responses. Campbell systematic reviews follow carefully structured, peer‐ reviewed procedures to produce high‐quality, theory‐based evaluations of social and economic policies and programmes. They address real‐world problems, often in partnership with relevant stakeholders, and seek to answer what works, why and for whom. Our 12 coordinating groups provide broad coverage of social issues, including ageing, business and management, climate solutions, crime and justice, disability, education, international development, knowledge translation and implementation, methods, nutrition and food systems and social welfare. And our international editorial board supervises the process in order to produce rigorous evidence syntheses and strategic partnerships that encourage their timely consideration for policy. Campbell systematic reviews have influenced national policy discussions on over 40 topics. They inform international guidelines and support the design and scaling‐up of dozens of evidence‐ based social and economic policies and programmes (Campbell Collaboration, 2020). Campbell also publishes evidence and gap maps, which provide a thorough overview of the body of evidence. They allow decision makers and planners to quickly identify the best available evidence on a topic, remaining evidence gaps, as well as suitable areas to be converted into living evidence reviews (Thomas et al., 2017). For example, the Campbell evidence and gap map on people with disabilities may be helpful to inform decisions about health, social engagement and employment for people with disabilities (Saran, White, & Kuper, 2020) in the aftermath of COVID‐19 stringency measures. With this editorial, we provide a virtual issue of 50 Campbell systematic reviews to inform the social and economic response to COVID‐19 (Figure 1). Some reviews have immediate relevance, including how to promote handwashing (De Buck et al., 2017), distribute cash in emergency settings, provide nutrition outreach, intervene for the safety of women and children and implement evidence‐based policing. Lockdown measures put pressure on families. We can learn from the large number of reviews on family functioning such as promoting the well‐being of children exposed to intimate partner violence (Latzman, Casanueva, Brinton, & Forman‐Hoffman, 2019). Reviews provide guidance to support vulnerable populations including the elderly, and others needing assistance in daily living. Other reviews cover programmes to strengthen the social safety net, for example, in food security, cash transfers and care homes. As economies reopen, Campbell reviews offer ideas on how best to get people back to work, including labour activation measures such as youth employment (Kluve et al., 2017), promoting entrepreneurship and providing vocational training. With global shutdowns in food processing plants and agriculture, we need to increase food production and availability through transport, improving retail access and outreach to difficult‐to‐reach areas such as urban slums. Campbell reviews highlight the effects of technological support for farmers, training and contract farming. Campbell reviews inform how to restructure government services such as schools, community services and prisons to support continued social distancing. New evidence syntheses are needed in some areas to answer questions directly related to COVID‐19 policies; for example, evidence on the impacts of reopening of schools on disease burden,"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://onlinelibrary.wiley.com/doi/pdfdirect/10.1002/cl2.1107
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
