
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Is the Paris Agreement Effective? A systematic map of the evidence"
authors: 
  - K. Raiser
  - U. Kornek
  - C. Flachsland
  - William Lamb
date: 2020-08-07T00:00:00Z
doi: "10.1088/1748-9326/ab865c"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "The Paris Agreement (PA) sets out to strengthen the global response to climate change, setting targets for mitigation, adaptation, and finance, and establishing mechanisms through which to achieve these targets. The effectiveness of the PA’s mechanisms in achieving its targets, however, has been questioned. This review systematically maps the peer-reviewed literature on the PA, categorizing the available evidence on whether or not the ‘Paris Regime’ can be effective. We split our analysis into three methodologically distinct sections: first we categorize the literature according to the mechanisms being studied. We find a diverse body of literature, albeit with a clear focus on mitigation, and identify adaptation and capacity building to be clear gaps. Second, we carry out a content analysis, identifying common drivers of, barriers to, and recommendations for effectiveness. Here we find mixed evidence, with potential drivers often qualified by more concrete barriers. Thirdly, we use scientometrics to identify six research clusters. These cover loss and damage, finance, legal issues, international politics, experimental evidence, and studies on tracking progress on the PA’s targets. We conclude with a narrative discussion of our findings, presenting three central themes. First, transparency is widely considered a precondition for the PA to be institutionally effective. However, a lack of clear reporting standards and comparable information renders the PA’s transparency provisions ineffective. Second, environmental effectiveness relies on national ambition, of which there is currently too little. It remains unclear to which extent the Paris Regime structure itself can induce significant ratcheting-up of ambition. Finally, the PA facilitates the diffusion of norms, enables learning and the sharing of best practices. This production of shared norms provides the most promising avenue for overcoming the current lack of ambition. One of the primary successes of the PA is in providing a platform for the exchange of experiences and ideas."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
