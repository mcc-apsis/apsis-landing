
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Understanding different perspectives on economic growth and climate policy"
authors: 
  - Michael Jakob
  - William Lamb
  - J. Steckel
  - C. Flachsland
  - O. Edenhofer
date: 2020-08-27T00:00:00Z
doi: "10.1002/wcc.677"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "WIREs Climate Change"
publication_short: ""

abstract: "Should economic growth continue in a world threatened by the prospect of catastrophic climate change? The scientific and public debate has brought forth a broad spectrum of views and narratives on this question, ranging from neoclassical economics to degrowth. We argue that different positions can be attributed to underlying differences in views on (a) factors that determine human well‐being, (b) the feasibility and desirability of economic growth, (c) appropriate intervention points, and (d) preferences about governance and policy options. For each of these dimensions, we propose points of agreement on which a consensus between conflicting positions might be achieved. From this basis, we distill a sustainability transition perspective that could act as a basis for a renewed debate on how to align human well‐being with environmental sustainability."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://wires.onlinelibrary.wiley.com/doi/pdfdirect/10.1002/wcc.677
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
