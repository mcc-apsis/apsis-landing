
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Keeping up with the Patels: Conspicuous consumption drives the adoption of cars and appliances in India"
authors: 
  - A. Ramakrishnan
  - M. Kalkuhl
  - Sohail Ahmad
  - F. Creutzig
date: 2020-12-01T00:00:00Z
doi: "10.1016/j.erss.2020.101742"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Energy Research & Social Science"
publication_short: ""

abstract: "End-users base their consumption decisions not only on available budget and direct use value, but also on their social environment. The underlying social dynamics are particularly important in the case of consumer goods that implicate high future energy demand and are, hence, also key for climate mitigation. This paper investigates the impact of social factors, with a focus on ‘status perceptions’, on car and appliance ownerships by urban India households. Using two rounds of the household-level data from the India Human Development Survey (IHDS, 2005 and 2012), we test for the impact of social factors in addition to economic, demographic, locational, and housing on ownership levels. Starting with factor analysis to categorise appliances by their latent characteristics, we then apply the bivariate ordered probit model to identify drivers of consumption among the urban households. We find that while income and household demographics are predominant drivers of car and appliance uptake, the household’s perception of status, instrumented by a variable measuring expenditure on conspicuous consumption, emerges as a key social dimension influencing the uptake. The results indicate how households identify themselves in society influences their corresponding car and appliance consumption. A deeper understanding of status-based consumption is, therefore, essential to designing better demand-side solutions to low-carbon consumption."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
