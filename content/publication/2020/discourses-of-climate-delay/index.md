
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Discourses of climate delay"
authors: 
  - William Lamb
  - Giulio Mattioli
  - S. Levi
  - J. Roberts
  - S. Capstick
  - F. Creutzig
  - Jan Minx
  - Finn Müller-Hansen
  - Trevor Culhane
  - J. Steinberger
date: 2020-07-01T00:00:00Z
doi: "10.1017/sus.2020.13"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Global Sustainability"
publication_short: ""

abstract: "Non-technical summary ‘Discourses of climate delay’ pervade current debates on climate action. These discourses accept the existence of climate change, but justify inaction or inadequate efforts. In contemporary discussions on what actions should be taken, by whom and how fast, proponents of climate delay would argue for minimal action or action taken by others. They focus attention on the negative social effects of climate policies and raise doubt that mitigation is possible. Here, we outline the common features of climate delay discourses and provide a guide to identifying them. Technical summary Through our collective observations as social scientists studying climate change, we describe 12 climate delay discourses and develop a typology based on their underlying logic. Delay discourses can be grouped into those that: (1) redirect responsibility; (2) push non-transformative solutions; (3) emphasize the downsides of climate policies; or (4) surrender to climate change. These discourses are distinct from climate denialism, climate-impact scepticism and ad hominem attacks, but are often used in combination to erode public and political support for climate policies. A deeper investigation of climate delay discourses is necessary in order to understand their prevalence and to develop inoculation strategies that protect the public from their intended effects. Our typology enables scientists, climate advocates and policymakers to recognize and counter these arguments when they are used. We urge all proponents of climate action to address these common misrepresentations of the climate crisis and to better communicate the dramatic pace of global warming, the gravity of its impacts and the possibility of effective and just mitigation policies. Social media summary Discourses of climate delay: redirect responsibility, push non-transformative solutions, emphasize downsides, surrender."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.cambridge.org/core/services/aop-cambridge-core/content/view/7B11B722E3E3454BB6212378E32985A7/S2059479820000137a.pdf/div-class-title-discourses-of-climate-delay-div.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
