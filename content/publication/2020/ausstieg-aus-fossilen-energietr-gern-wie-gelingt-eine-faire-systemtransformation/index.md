
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Ausstieg aus fossilen Energieträgern - wie gelingt eine faire Systemtransformation"
authors: 
  - Elmar Kriegler
  - Ramona Gulde
  - Arwen Colell
  - C. Hirschhausen
  - Jan Minx
  - Pao-Yu Oei
  - Paola Yanguas-Parra
  - Nicolas Bauer
  - Hanna Brauers
  - Lisa Hanna Broska
  - Elke Groh
  - Achim Hagen
  - Karlo Hainsch
  - F. Holz
  - M. Hübler
  - M. Jakob
  - Mohammad M. Khabbazan
  - Marian Leimbach
  - Niccolo Manych
  - Mariza Montes de Oca León
  - Nils Ohlendorf
  - Sebastian Osorio
  - Michael Pahle
  - L. Reutter
  - Hawal Shamon
  - J. C. Steckel
  - Jessica Strefler
  - C. Vance
  - S. Vögele
  - Georg von Wangenheim
  - Paula Walk
  - I. Wittenberg
  - S. Zundel
date: 2020-01-01T00:00:00Z
doi: "10.2312/PIK.2020.004"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "https://publications.pik-potsdam.de/pubman/item/item_24547"
publication_short: ""

abstract: "Nico Bauer (PIK) | Hanna Brauers (DIW) | Lisa Hanna Broska (Forschungszentrum Jülich) Elke Groh (Uni Kassel) | Achim Hagen (HU Berlin) | Karlo Hainsch (TU Berlin) | Franziska Holz (DIW) Michael Hübler (Uni Gießen) | Michael Jakob (MCC) | Mohammad M. Khabbazan (TU Berlin/DIW) Marian Leimbach (PIK) | Niccolo Manych (MCC) | Mariza Montes de Oca León (DIW) Nils Ohlendorf (MCC) | Sebastian Osorio (PIK) | Michael Pahle (PIK) | Leo Reutter (Uni Kassel) Hawal Shamon (Forschungszentrum Jülich) | Jan Steckel (MCC) | Jessica Strefler (PIK) Colin Vance (RWI) | Stefan Vögele (Forschungszentrum Jülich) | Georg von Wangenheim (Uni Kassel) Paula Walk (TU Berlin) | Inga Wittenberg (Uni Magdeburg) | Stefan Zundel (TU Cottbus-Senftenberg) Dialog zur Klimaökonomie"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
