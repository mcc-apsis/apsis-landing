
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Changes in urban green spaces’ value perception: A meta-analytic benefit transfer function for European cities"
authors: 
  - Francesca Diluiso
  - G. Guastella
  - S. Pareglio
date: 2020-10-23T00:00:00Z
doi: "10.1016/j.landusepol.2020.105116"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Land Use Policy"
publication_short: ""

abstract: "We estimate a meta-regression of the willingness to pay (WTP) for green spaces in European urban and peri-urban areas obtained from contingent valuation (CV) studies to construct a benefit transfer (BT) function. Compared to previous studies that estimated meta-regression for urban green spaces, we introduce two main novelties. Firstly, we focus exclusively on Europe to reduce the heterogeneity due to unobservable factors that affect the estimates and their transferability. Secondly, we use urban socio-economic indicators and land-use data derived from terrestrial observations as proxies for the supply and demand of ecosystem services at the local level, allowing interactions between these variables and the specific characteristics of the site valued. We demonstrate that both approaches improve the accuracy of the estimates and, hence, the validity of the transferred values."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
