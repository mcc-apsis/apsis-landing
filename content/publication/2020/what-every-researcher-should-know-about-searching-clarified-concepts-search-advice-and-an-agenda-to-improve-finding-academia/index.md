
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "What every researcher should know about searching – clarified concepts, search advice, and an agenda to improve finding in academia"
authors: 
  - Michael Gusenbauer
  - Neal R Haddaway
date: 2020-10-08T00:00:00Z
doi: "10.1002/jrsm.1457"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Research Synthesis Methods"
publication_short: ""

abstract: "We researchers have taken searching for information for granted for far too long. The COVID‐19 pandemic shows us the boundaries of academic searching capabilities, both in terms of our know‐how and of the systems we have. With hundreds of studies published daily on COVID‐19, for example, we struggle to find, stay up‐to‐date, and synthesize information—all hampering evidence‐informed decision making. This COVID‐19 information crisis is indicative of the broader problem of information overloaded academic research. To improve our finding capabilities, we urgently need to improve how we search and the systems we use."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://onlinelibrary.wiley.com/doi/pdfdirect/10.1002/jrsm.1457
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
