
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "What are the social outcomes of climate policies? A systematic map and review of the ex-post literature"
authors: 
  - William Lamb
  - M. Antal
  - Katharina Bohnenberger
  - Lina I. Brand-Correa
  - Finn Müller-Hansen
  - Michael Jakob
  - Jan Minx
  - K. Raiser
  - Laurence Williams
  - B. Sovacool
date: 2020-10-14T00:00:00Z
doi: "10.1088/1748-9326/abc11f"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environment Research Letters"
publication_short: ""

abstract: "It is critical to ensure climate and energy policies are just, equitable and beneficial for communities, both to sustain public support for decarbonisation and address multifaceted societal challenges. Our objective in this article is to examine the diverse social outcomes that have resulted from climate policies, in varying contexts worldwide, over the past few decades. We review 203 ex-post climate policy assessments that analyse social outcomes in the literature. We systematically and comprehensively map out this work, identifying articles on carbon, energy and transport taxes, feed-in-tariffs, subsidies, direct procurement policies, large renewable deployment projects, and other regulatory and market-based interventions. We code each article in terms of their studied social outcomes and effects, with a focus on electricity access, energy affordability, community cohesion, employment, distributional and equity issues, livelihoods and poverty, procedural justice, subjective well-being and drudgery. Our analysis finds that climate and energy policies often fall short of delivering positive social outcomes. Nonetheless, across country contexts and policy types there are manifold examples of climate policymaking that does deliver on both social and climate goals. This requires attending to distributive and procedural justice in policy design, and making use of appropriate mechanisms to ensure that policy costs and benefits are fairly shared. We emphasize the need to further advance ex-post policy assessments and learn about what policies work for a just transition."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
