
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Biophysical and economic limits to negative CO2 emissions"
authors: 
  - Pete Smith
  - S. Davis
  - F. Creutzig
  - S. Fuss
  - Jan Minx
  - Jan Minx
  - B. Gabrielle
  - Etsushi Kato
  - R. Jackson
  - A. Cowie
  - E. Kriegler
  - D. P. Vuuren
  - D. Vuuren
  - J. Rogelj
  - J. Rogelj
  - P. Ciais
  - J. Milne
  - J. Canadell
  - D. McCollum
  - G. Peters
  - R. Andrew
  - V. Krey
  - G. Shrestha
  - P. Friedlingstein
  - T. Gasser
  - A. Grubler
  - W. Heidug
  - M. Jonas
  - C. Jones
  - F. Kraxner
  - E. Littleton
  - J. Lowe
  - J. Moreira
  - N. Nakicenovic
  - M. Obersteiner
  - A. Patwardhan
  - M. Rogner
  - E. Rubin
  - Ayyoob Sharifi
  - A. Torvanger
  - Y. Yamagata
  - J. Edmonds
  - Cho Yongsung
date: 2015-01-01T00:00:00Z
doi: "10.1038/NCLIMATE2870"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "To have a >50% chance of limiting warming below 2 °C, most recent scenarios from integrated assessment models (IAMs) require large-scale deployment of negative emissions technologies (NETs). These are technologies that result in the net removal of greenhouse gases from the atmosphere. We quantify potential global impacts of the different NETs on various factors (such as land, greenhouse gas emissions, water, albedo, nutrients and energy) to determine the biophysical limits to, and economic costs of, their widespread application. Resource implications vary between technologies and need to be satisfactorily addressed if NETs are to have a significant role in achieving climate goals."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://dspace.library.uu.nl/bitstream/handle/1874/382262/nclimate2870.pdf?sequence=1&isAllowed=y
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
