
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Transport: A roadblock to climate change mitigation?"
authors: 
  - F. Creutzig
  - P. Jochem
  - O. Edelenbosch
  - Linus Mattauch
  - D. Vuuren
  - D. McCollum
  - Jan Minx
date: 2015-11-20T00:00:00Z
doi: "10.1126/science.aac8033"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Science"
publication_short: ""

abstract: "Urban mobility solutions foster climate mitigation Global emissions scenarios studies, such as those informing the Intergovernmental Panel on Climate Change (IPCC) 5th Assessment Report (AR5), highlight the importance of the transport sector for climate change mitigation—along with the difficulties of achieving deep reductions therein (1) [supplementary materials (SM)]. Transport is responsible for about 23% of total energy-related CO2 emissions worldwide (2). The sector is growing more rapidly than most others, with emissions projected to double by 2050. Global scenario studies, specifically those produced by integrated assessment models (IAMs), communicate aggregate mitigation potentials by sectors in IPCC reports. Yet recent evidence indicates that emissions may be reduced further than these global scenario studies suggest—if policy-makers use the full suite of policies at their disposal."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://publikationen.bibliothek.kit.edu/1000055150/16704868
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
