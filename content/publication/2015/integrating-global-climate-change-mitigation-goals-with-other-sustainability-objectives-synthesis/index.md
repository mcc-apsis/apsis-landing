
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Integrating Global Climate Change Mitigation Goals with Other Sustainability Objectives: A Synthesis"
authors: 
  - C. V. Stechow
  - D. McCollum
  - K. Riahi
  - Jan Minx
  - E. Kriegler
  - D. Vuuren
  - J. Jewell
  - C. Robledo-Abad
  - E. Hertwich
  - M. Tavoni
  - S. Mirasgedis
  - O. Lah
  - J. Roy
  - Y. Mulugetta
  - N. Dubash
  - J. Bollen
  - D. Ürge-Vorsatz
  - O. Edenhofer
date: 2015-11-05T00:00:00Z
doi: "10.1146/ANNUREV-ENVIRON-021113-095626"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Annual Review of Environment and Resources Volume 40"
publication_short: ""

abstract: "Achieving a truly sustainable energy transition requires progress across multiple dimensions beyond climate change mitigation goals. This article reviews and synthesizes results from disparate strands of literature on the coeffects of mitigation to inform climate policy choices at different governance levels. The literature documents many potential cobenefits of mitigation for nonclimate objectives, such as human health and energy security, but little is known about their overall welfare implications. Integrated model studies highlight that climate policies as part of well-designed policy packages reduce the overall cost of achieving multiple sustainability objectives. The incommensurability and uncertainties around the quantification of coeffects become, however, increasingly pervasive the more the perspective shifts from sectoral and local to economy wide and global, the more objectives are analyzed, and the more the results are expressed in economic rather than nonmonetary terms. Different strings of e..."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://dspace.library.uu.nl/bitstream/handle/1874/323653/26.pdf?sequence=1&isAllowed=y
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
