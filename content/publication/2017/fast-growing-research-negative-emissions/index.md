
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Fast growing research on negative emissions"
authors: 
  - Jan Minx
  - William Lamb
  - Max Callaghan
  - L. Bornmann
  - S. Fuss
date: 2017-03-01T00:00:00Z
doi: "10.1088/1748-9326/aa5ee5"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "Generating negative emissions by removing carbon dioxide from the atmosphere is a key requirement for limiting global warming to well below 2 °C, or even 1.5 °C, and therefore for achieving the long-term climate goals of the recent Paris Agreement. Despite being a relatively young topic, negative emission technologies (NETs) have attracted growing attention in climate change research over the last decade. A sizeable body of evidence on NETs has accumulated across different fields that is by today too large and too diverse to be comprehensively tracked by individuals. Yet, understanding the size, composition and thematic structure of this literature corpus is a crucial pre-condition for effective scientific assessments of NETs as, for example, required for the new special report on the 1.5 °C by the Intergovernmental Panel on Climate Change (IPCC). In this paper we use scientometric methods and topic modelling to identify and characterize the available evidence on NETs as recorded in the Web of Science. We find that the development of the literature on NETs has started later than for climate change as a whole, but proceeds more quickly by now. A total number of about 2900 studies have accumulated between 1991 and 2016 with almost 500 new publications in 2016. The discourse on NETs takes place in distinct communities around energy systems, forests as well as biochar and other soil carbon options. Integrated analysis of NET portfolios—though crucial for understanding how much NETs are possible at what costs and risks—are still in their infancy and do not feature as a theme across the literature corpus. Overall, our analysis suggests that NETs research is relatively marginal in the wider climate change discourse despite its importance for global climate policy."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
