
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Better data for assessing local climate policies"
authors: 
  - Jan Minx
date: 2017-05-15T00:00:00Z
doi: "10.1088/1748-9326/aa6e03"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "There is a growing recognition that successfully managing the transformation towards a net zero carbon world as established through the climate goals of the recent Paris Agreement is an exercise of multilevel governance. Only through strong action at all levels, with close coordination across actors from the international scale down to countries, firms and individuals, might such ambitious goals be met (figure 1). Triggered by the slow progress in international climate policy over the last two decades, cities and local governments have teamed-up to combat climate change from the bottom up: thousands have developed and implemented local climate action plans. Yet, little is known about the impact those measures had on reducing emissions (Seto et al 2014)."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
