
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "From Targets to Action: Rolling up our Sleeves after Paris"
authors: 
  - B. Knopf
  - S. Fuss
  - Finn Müller-Hansen
  - F. Creutzig
  - Jan Minx
  - O. Edenhofer
date: 2017-01-30T00:00:00Z
doi: "10.1002/gch2.201600007"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Global Challenges"
publication_short: ""

abstract: "At the United Nations Climate Change Conference in Paris in 2015 ambitious targets for responding to the threat of climate change have been set: limiting global temperature increase to “well below 2 °C […] and to pursue efforts to limit the temperature increase to 1.5 °C”. However, calculating the CO2 budget for 1.5 °C, it becomes clear that there is nearly no room left for future emissions. Scenarios suggest that negative emission technologies will play an even more important role for 1.5 °C than they already play for 2 °C. Especially against this background the feasibility of the target(s) is hotly debated, but this debate does not initiate the next steps that are urgently needed. Already the negotiations have featured the move from targets to implementation which is needed in the coming decade. Most importantly, there is an urgent need to develop and implement instruments that incentivize the rapid decarbonization. Moreover, it needs to be worked out how to link the climate and development agenda and prevent a buildup of coal power causing lock‐in effects. Short term entry points into climate policy should now be in the focus instead of the fruitless debate on the feasibility of targets."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
