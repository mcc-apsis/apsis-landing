
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Human well-being and climate change mitigation"
authors: 
  - William Lamb
  - J. Steinberger
date: 2017-08-17T00:00:00Z
doi: "10.1002/wcc.485"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Wiley Interdisciplinary Reviews: Climate Change"
publication_short: ""

abstract: "Climate change mitigation research is fundamentally motivated by the preservation of human lives and the environmental conditions which enable them. However, the field has to date rather superficial in its appreciation of theoretical claims in well‐being thought, with deep implications for the framing of mitigation priorities, policies, and research. Major strands of well‐being thought are hedonic well‐being—typically referred to as happiness or subjective well‐being—and eudaimonic well‐being, which includes theories of human needs, capabilities, and multidimensional poverty. Aspects of each can be found in political and procedural accounts such as the Sustainable Development Goals. Situating these concepts within the challenges of addressing climate change, the choice of approach is highly consequential for: (1) understanding inter‐ and intra‐generational equity; (2) defining appropriate mitigation strategies; and (3) conceptualizing the socio‐technical provisioning systems that convert biophysical resources into well‐being outcomes. Eudaimonic approaches emphasize the importance of consumption thresholds, beyond which dimensions of well‐being become satiated. Related strands of well‐being and mitigation research suggest constraining consumption to within minimum and maximum consumption levels, inviting normative discussions on the social benefits, climate impacts, and political challenges associated with a given form of provisioning. The question of how current socio‐technical provisioning systems can be shifted towards low‐carbon, well‐being enhancing forms constitutes a new frontier in mitigation research, involving not just technological change and economic incentives, but wide‐ranging social, institutional, and cultural shifts. WIREs Clim Change 2017, 8:e485. doi: 10.1002/wcc.485"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://onlinelibrary.wiley.com/doi/pdfdirect/10.1002/wcc.485
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
