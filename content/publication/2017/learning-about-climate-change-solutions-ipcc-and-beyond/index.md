
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Learning about climate change solutions in the IPCC and beyond"
authors: 
  - Jan Minx
  - Max Callaghan
  - William Lamb
  - Jennifer Garard
  - O. Edenhofer
date: 2017-11-01T00:00:00Z
doi: "10.1016/J.ENVSCI.2017.05.014"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Science & Policy"
publication_short: ""

abstract: "There has been much debate about the assessment process of the Intergovernmental Panel on Climate Change (IPCC). Yet two of the most fundamental challenges that directly threaten the ability of the IPCC to fulfill its mandate have been largely neglected so far. Firstly, the magnitude and rapid expansion of the climate change literature makes it increasingly impossible for the IPCC to conduct comprehensive and transparent assessments without major innovations in assessment practices and tools. Secondly, the structure, organization and scientific practices across the social sciences and humanities prohibit systematic learning on climate change solutions and increasingly limit the policy-relevance of IPCC assessments. We highlight the need for responses along three avenues to prepare the IPCC for continued success in the future: 1) IPCC assessments must make better use of big-data methods and available computational power to assess the growing body of literature and ensure comprehensiveness; 2) systematic review practices need to be enshrined into IPCC procedures to ensure adequate focus and transparency in its assessments; 3) a synthetic research culture needs to be established in the social sciences and humanities in order to foster knowledge accumulation and learning on climate solutions in the future. As policymakers become more interested in understanding solutions, the future prospects of global environmental assessment enterprises will depend heavily on a successful transformation within the social sciences and humanities towards systematic knowledge generation. This article is part of a special issue on solution-oriented Global Environmental Assessments. "

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
