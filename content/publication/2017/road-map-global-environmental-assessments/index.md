
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A road map for global environmental assessments"
authors: 
  - Martin Kowarsch
  - J. Jabbour
  - C. Flachsland
  - M. Kok
  - R. Watson
  - P. Haas
  - Jan Minx
  - J. Alcamo
  - Jennifer Garard
  - P. Riousset
  - L. Pintér
  - C. Langford
  - Y. Yamineva
  - C. V. Stechow
  - J. O’Reilly
  - O. Edenhofer
date: 2017-05-31T00:00:00Z
doi: "10.1038/NCLIMATE3307"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "Increasing demand for solution-oriented environmental assessments brings significant opportunities and challenges at the science–policy–society interface. Solution-oriented assessments should enable inclusive deliberative learning processes about policy alternatives and their practical consequences. "

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
