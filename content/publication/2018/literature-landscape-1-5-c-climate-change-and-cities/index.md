
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The literature landscape on 1.5°C climate change and cities"
authors: 
  - William Lamb
  - Max Callaghan
  - F. Creutzig
  - R. Khosla
  - Jan Minx
date: 2018-01-01T00:00:00Z
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Current Opinion in Environmental Sustainability"
publication_short: ""

abstract: "Cities are key for achieving the 1.5 °C warming limit of the Paris Agreement. However, synthesizing policy insights from the urban literature is a challenge, due to its rapid growth, breadth of topics and relative lack of assessments so far. Here we introduce methods from computational linguistics to build a systematic overview of research on transport, buildings, waste management and urban form. We find that the epistemic core of the mitigation-focused urban literature is currently centered on urban form and emissions accounting, while extensive research into demand-side options remain overlooked, including congestion and parking polices, active travel, and waste management. In the IPCC Special Report on 1.5 °C, and for meeting the target itself, all such city-scale opportunities need to be examined. "

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
