
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Divestment prevails over the green paradox when anticipating strong future climate policies"
authors: 
  - N. Bauer
  - C. Mcglade
  - Jérôme Hilaire
  - P. Ekins
date: 2018-01-29T00:00:00Z
doi: "10.1038/s41558-017-0053-1"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Nature Climate Change"
publication_short: ""

abstract: "Fossil fuel market dynamics will have a significant impact on the effectiveness of climate policies1. Both fossil fuel owners and investors in fossil fuel infrastructure are sensitive to climate policies that threaten their natural resource endowments and production capacities2,3,4, which will consequently affect their near-term behaviour. Although weak in near-term policy commitments5,6, the Paris Agreement on climate7 signalled strong ambitions in climate change stabilization. Many studies emphasize that the 2 °C target can still be achieved even if strong climate policies are delayed until 20308,9,10. However, sudden implementation will have severe consequences for fossil fuel markets and beyond and these studies ignore the anticipation effects of owners and investors. Here we use two energy–economy models to study the collective influence of the two central but opposing anticipation arguments, the green paradox11 and the divestment effect12, which have, to date, been discussed only separately. For a wide range of future climate policies, we find that anticipation effects, on balance, reduce CO2 emissions during the implementation lag. This is because of strong divestment in coal power plants starting ten years ahead of policy implementation. The green paradox effect is identified, but is small under reasonable assumptions. "

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://discovery.ucl.ac.uk/10044415/1/Ekins%20manuscript_final_clean.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
