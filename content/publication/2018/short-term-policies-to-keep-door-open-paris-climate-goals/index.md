
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Short term policies to keep the door open for Paris climate goals"
authors: 
  - E. Kriegler
  - C. Bertram
  - T. Kuramochi
  - Michael Jakob
  - M. Pehl
  - M. Stevanović
  - N. Höhne
  - G. Luderer
  - Jan Minx
  - Hanna Fekete
  - Jérôme Hilaire
  - Lisa Luna
  - A. Popp
  - J. Steckel
  - S. Sterl
  - A. Yalew
  - J. Dietrich
  - O. Edenhofer
date: 2018-07-01T00:00:00Z
doi: "10.1088/1748-9326/aac4f1"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "Climate policy needs to account for political and social acceptance. Current national climate policy plans proposed under the Paris Agreement lead to higher emissions until 2030 than cost-effective pathways towards the Agreements’ long-term temperature goals would imply. Therefore, the current plans would require highly disruptive changes, prohibitive transition speeds, and large long-term deployment of risky mitigation measures for achieving the agreement’s temperature goals after 2030. Since the prospects of introducing the cost-effective policy instrument, a global comprehensive carbon price in the near-term, are negligible, we study how a strengthening of existing plans by a global roll-out of regional policies can ease the implementation challenge of reaching the Paris temperature goals. The regional policies comprise a bundle of regulatory policies in energy supply, transport, buildings, industry, and land use and moderate, regionally differentiated carbon pricing. We find that a global roll-out of these policies could reduce global CO2 emissions by an additional 10 GtCO2eq in 2030 compared to current plans. It would lead to emissions pathways close to the levels of cost-effective likely below 2 °C scenarios until 2030, thereby reducing implementation challenges post 2030. Even though a gradual phase-in of a portfolio of regulatory policies might be less disruptive than immediate cost-effective carbon pricing, it would perform worse in other dimensions. In particular, it leads to higher economic impacts that could become major obstacles in the long-term. Hence, such policy packages should not be viewed as alternatives to carbon pricing, but rather as complements that provide entry points to achieve the Paris climate goals."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
