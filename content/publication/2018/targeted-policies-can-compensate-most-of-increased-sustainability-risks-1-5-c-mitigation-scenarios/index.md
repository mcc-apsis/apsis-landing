
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Targeted policies can compensate most of the increased sustainability risks in 1.5 °C mitigation scenarios"
authors: 
  - C. Bertram
  - G. Luderer
  - A. Popp
  - Jan Minx
  - William Lamb
  - M. Stevanović
  - Florian Humpenöder
  - A. Giannousakis
  - E. Kriegler
date: 2018-06-01T00:00:00Z
doi: "10.1088/1748-9326/aac3ec"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "Meeting the 1.5 °C goal will require a rapid scale-up of zero-carbon energy supply, fuel switching to electricity, efficiency and demand-reduction in all sectors, and the replenishment of natural carbon sinks. These transformations will have immediate impacts on various of the sustainable development goals. As goals such as affordable and clean energy and zero hunger are more immediate to great parts of global population, these impacts are central for societal acceptability of climate policies. Yet, little is known about how the achievement of other social and environmental sustainability objectives can be directly managed through emission reduction policies. In addition, the integrated assessment literature has so far emphasized a single, global (cost-minimizing) carbon price as the optimal mechanism to achieve emissions reductions. In this paper we introduce a broader suite of policies—including direct sector-level regulation, early mitigation action, and lifestyle changes—into the integrated energy-economy-land-use modeling system REMIND-MAgPIE. We examine their impact on non-climate sustainability issues when mean warming is to be kept well below 2 °C or 1.5 °C. We find that a combination of these policies can alleviate air pollution, water extraction, uranium extraction, food and energy price hikes, and dependence on negative emissions technologies, thus resulting in substantially reduced sustainability risks associated with mitigating climate change. Importantly, we find that these targeted policies can more than compensate for most sustainability risks of increasing climate ambition from 2 °C to 1.5 °C."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
