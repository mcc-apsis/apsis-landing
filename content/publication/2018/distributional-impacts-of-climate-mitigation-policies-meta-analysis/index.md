
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Distributional Impacts of Climate Mitigation Policies - a Meta-Analysis"
authors: 
  - N. Ohlendorf
  - Michael Jakob
  - Jan Minx
  - Carsten Schröder
  - J. Steckel
date: 2018-12-01T00:00:00Z
doi: "10.2139/ssrn.3299337"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "DIW Discussion Paper"
publication_short: ""

abstract: "Understanding the distributional impacts of market-based climate policies is crucial to design economically efficient climate change mitigation policies that are socially acceptable and avoid adverse impacts on the poor. Empirical studies that examine the distributional impacts of carbon pricing and fossil fuel subsidy reforms in different countries arrive at ambiguous results. To systematically determine the sources of variation between these outcomes, we apply an ordered probit meta-analysis framework. Based on a comprehensive, systematic and transparent screening of the literature, our sample comprises 53 empirical studies containing 183 effects in 39 countries. Results indicate a significantly increased likelihood of progressive distributional outcomes for studies on lower income countries and transport sector policies. The same applies to study designs that consider indirect effects, behavioral adjustments of consumers or lifetime income proxies. Future research on different types of revenue recycling schemes and lower income countries would further contribute to the literature."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: http://www.diw.de/documents/publikationen/73/diw_01.c.610188.de/dp1776.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
