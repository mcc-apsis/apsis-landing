
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Connecting the sustainable development goals by their energy inter-linkages"
authors: 
  - D. McCollum
  - L. G. Echeverri
  - S. Busch
  - S. Pachauri
  - S. Parkinson
  - J. Rogelj
  - V. Krey
  - Jan Minx
  - M. Nilsson
  - Anne-Sophie Stevance
  - K. Riahi
date: 2018-03-15T00:00:00Z
doi: "10.1088/1748-9326/aaafe3"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-01-01

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Environmental Research Letters"
publication_short: ""

abstract: "The United Nations’ Sustainable Development Goals (SDGs) provide guide-posts to society as it attempts to respond to an array of pressing challenges. One of these challenges is energy; thus, the SDGs have become paramount for energy policy-making. Yet, while governments throughout the world have already declared the SDGs to be ‘integrated and indivisible’, there are still knowledge gaps surrounding how the interactions between the energy SDG targets and those of the non-energy-focused SDGs might play out in different contexts. In this review, we report on a large-scale assessment of the relevant energy literature, which we conducted to better our understanding of key energy-related interactions between SDGs, as well as their context-dependencies (relating to time, geography, governance, technology, and directionality). By (i) evaluating the nature and strength of the interactions identified, (ii) indicating the robustness of the evidence base, the agreement of that evidence, and our confidence in it, and (iii) highlighting critical areas where better understanding is needed or context dependencies should be considered, our review points to potential ways forward for both the policy making and scientific communities. First, we find that positive interactions between the SDGs outweigh the negative ones, both in number and magnitude. Second, of relevance for the scientific community, in order to fill knowledge gaps in critical areas, there is an urgent need for interdisciplinary research geared toward developing new data, scientific tools, and fresh perspectives. Third, of relevance for policy-making, wider efforts to promote policy coherence and integrated assessments are required to address potential policy spillovers across sectors, sustainability domains, and geographic and temporal boundaries. The task of conducting comprehensive science-to-policy assessments covering all SDGs, such as for the UN’s Global Sustainable Development Report, remains manageable pending the availability of systematic reviews focusing on a limited number of SDG dimensions in each case."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

share: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

        
