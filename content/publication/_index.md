---
title: Publications

# Listing view
view: citation

# Optional banner image (relative to `assets/media/` folder).
banner:
  caption: ''
  image: ''
---

*The publication list might not be complete or up-to-date.*  
