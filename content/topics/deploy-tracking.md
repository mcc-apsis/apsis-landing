---
title: Tracking CDR deployments
date: 2023-10-26
categories:
  - Thesis proposal
authors:
  - Tim Repke
---

Starting with a list of CDR companies (e.g. cdr.fyi, cdratlas, https://carbonplan.org/...) and CDR news (e.g. mailing list), find news articles, grey literature, tweets, … mentioning those companies. 
Can we extract information such as actual sequestration, projected scale, and investments? 
Construct a database/knowledge graph of bits of information and sources.

Useful skills: none for manual feasibility study, computer linguistic/NLP and data engineering skills required for automation;
Useful prior knowledge: carbon dioxide removal, media analysis
Contact: Tim
