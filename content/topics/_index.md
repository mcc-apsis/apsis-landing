---
title: Thesis topic ideas

# Listing view
view: card

# Optional banner image (relative to `assets/media/` folder).
banner:
  caption: ''
  image: ''
---
We are always looking for bright minds who want to work with us.
If you are interested in what you see (or are inspired by the outlines), get in touch, we are happy to supervise you.
You may also consider having a look at our open positions...
