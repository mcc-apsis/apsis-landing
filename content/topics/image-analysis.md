---
title: Visual metaphors for CDR and/or climate change
date: 2023-10-26
categories:
  - Thesis proposal
authors:
  - Tim Repke
---



What images do different media outlets choose for their content on climate change or different CDR methods? Looking at cover images in news platforms or attachments in Tweets (or other social media), run object detection on those and maybe some “mood” measures to compare common themes. Are there common visual metaphors (e.g. natural solutions always with rainforest, other things with smoke stacks or clouds)?
Useful skills: basic programming to retrieve and manage data and apply image classification
Contact: Tim
