---
title: 'CDR on Twitter: links to external sources and/or qualitative analysis'
date: 2023-10-26
categories:
  - Thesis proposal
authors:
  - Finn Müller-Hansen
---


## Research questions
* What links and sources do users share on Twitter about CDR? 
* What kinds of contents are shared?

## Useful skills
python scripting for web crawling and data analysis, NLP

## Useful prior knowledge
carbon dioxide removal, media analysis
