---
title: 'Technology scale up: comparing different growth measures for emerging technologies' 
date: 2023-10-26
categories:
  - Thesis proposal
authors:
  - Finn Müller-Hansen
---


Research questions: Which aspects of technology scale up do different measures highlight? Which aspects of growth trajectories do they focus on? On which indicators are they applicable? How well are they applicable to incomplete and noisy data?
Useful skills: mathematical modeling, data analysis (curve fitting)
Useful prior knowledge: innovation, development, scaling and diffusion of emerging technologies
Related literature: Nemet et al. (forthcoming), Wilson papers, Cherp et al.
Contact: Finn
