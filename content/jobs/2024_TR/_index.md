---
title: Data Engineering and Data Science
date: 2024-09-30
categories:
  - Job ad
authors:
  - Tim Repke
summary: "Create next-generation digital evidence synthesis tools (with AI)"
---
[Complete ad](ad.pdf)

We are looking for students to support us in our research endeavours or the maintenance and improvement of our screening platform.
In our working group at the MCC (soon to be part of PIK) we focus on research syntheses (summarising existing literature on specific topics, e.g. as preparatory work or direct contribution to the IPCC's World Climate Report). Due to the ever increasing volume of publications, we (partially) automate this process with the help of AI. At this point, we have several million articles and several hundred thousand manual annotations on our platform.

Students in our group can choose their work relatively freely based on their interests and expertise: from close monitoring and support for project partners who use our platform (analysing data, automating processes, ...), development of the platform (frontend or backend), to driving forward own projects that improve and evaluate the secure and robust use of ‘digital evidence synthesis tools’ (lots of potential for master's theses ...)

* 8-20h/week
* TV-Stud III (12.96-13.84€/h)
* Berlin Schöneberg on the EUREF Campus

If you are interested, get in touch with Tim Repke.
