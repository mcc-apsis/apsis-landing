---
title: Carbon Removal and Innovation Research
date: 2024-08-20
categories:
  - Job ad
tags:
  - Job ad
authors:
  - Finn Müller-Hansen
summary: "Contribute to data analyses and innovation research in the field of carbon removal as part of the GENIE and State of CDR project"
---
[Complete ad](./ad.pdf)

The successful candidate will be located at MCC in Berlin and contribute to the analysis of datasets
with the aim of better understanding research and development of carbon removal. Depending on the
project phase and the available skills, this might include:
* Conduct statistical analyses of complex datasets such as research funding, patenting and
publication data; 
* Create data visualizations for scientific publications; 
* Annotate datasets for validating machine learning models; 
* Support the writing of publications and reports
