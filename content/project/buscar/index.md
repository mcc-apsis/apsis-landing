---
title: "Stopping criteria for prioritised screening"
summary: "The stopping criteria offers a reliable method to decide when to stop screening during machine learning-prioritised screening for a systematic review, or for document review in ediscovery."
authors: ["Max Callaghan", "Finn Müller-Hansen"]
tags: [data,tools]
categories: []
date: 2023-09-25T17:39:13+02:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: "https://mcallaghan.github.io/buscarR/"
url_code: "https://github.com/mcallaghan/buscarpy"
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
The stopping criteria offers a reliable method to decide when to stop screening during machine learning-prioritised 
screening for a systematic review, or for document review in ediscovery.

Such a method is vital in order to use machine learning safely (by estimating the risk of missing documents). 
Our method describes statistical confidence levels that any given level of recall has not been achieved. 
As such, it offers a robust data-driven method to decide when to stop, and a clear and transparent way of
communicating the risks of stopping at any given point. Read vignette("stopping-criteria") for more details on how
this works in the package, or check out the original paper for a full description of the criteria, including theory and evaluations.

### References & Source code

Buscar, short for Biased Urn based Stopping Criteria for technology Assisted Review, 
is an R and Python package, and a web app, implementing the stopping criteria described in

> Callaghan, Max, and Finn Müller-Hansen. 2020. *“Statistical Stopping Criteria for Automated Screening in Systematic Reviews.”* Systematic Reviews. https://doi.org/10.21203/rs.2.18218/v2.

* BuscarR [R package](https://mcallaghan.github.io/buscarR/)
* BuscarPy [Python package](https://github.com/mcallaghan/buscarpy)
* Buscar [Web application](https://mcallaghan.github.io/buscar-app/)