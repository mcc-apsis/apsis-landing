---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "NACSOS 2"
summary: "Our new internal platform for managing text corpora, screen and code documents for large-scale systematic maps and reviews."
authors: ["Tim Repke", "Max Callaghan"]
tags: [tools]
categories: []
date: 2023-08-03T17:39:13+02:00
lastmod: 2024-08-28T17:39:13+02:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: "https://gitlab.pik-potsdam.de/mcc-apsis/nacsos"
url_pdf: "2405.04621v1.pdf"
url_slides: "20230929_WWCS_Potsdam_AI_Workshop.pdf"
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
NACSOS 2 is our internal platform to perform large-scale systematic maps and reviews with machine learning assistance.

For more information, please check out the documentation or other supplemental materials linked below.

### Resources

* Platform: https://apsis.mcc-berlin.net/nacsos/ 
* NACSOS Documentation: https://apsis.mcc-berlin.net/nacsos-docs/ 
* Source code: https://gitlab.pik-potsdam.de/mcc-apsis/nacsos
* Intro slides: [Slides](20230929_WWCS_Potsdam_AI_Workshop.pdf)
* Working paper: [arXiv](2405.04621v1.pdf)

