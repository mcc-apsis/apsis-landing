---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "DESTinY: Digital Evidence Synthesis Tool InnovationYielding Improvements in Climate & Health"
summary: "Large research consortium"
authors: ["Jan Minx", "Max Callaghan", "Tim Repke"]
tags: [projects]
categories: []
date: 2024-08-20T17:39:13+02:00
#lastmod: 2024-08-28T17:39:13+02:00

# Optional external URL for project (replaces project detail page).
external_link: 

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

#url_web: "https://climateliterature.org"
url_code:
url_pdf: 
url_slides: 
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
DESTinY is a large consortium lead by MCC/PIK in collaboration with LSHTM, UCL, ASCEND, ACRES, Future Evidence Foundation, eBASE, Campbell Collaboration, Cochrane, and many other partners.
The project will officially start in January 2025.

{{< figure src="fig1.png" id="fig1" >}}

Climate change impacts already cause widespread human suffering affecting millions of people across the globe and health risks will further escalate with continued warming.
Urgent climate change mitigation and adaptation—often associated with substantial health co-benefits—are imperative to protect the health of future generations and to ensure climate-related health hazards stay within the capacity of health systems.
Decision makers urgently need access to timely knowledge to take impactful, evidence-based, health-centred climate action. 
We realise this through the co-production with evidence producers and tool developers. Living evidence provides a radical new model to rapidly fill relevant evidence gaps.
However the vast and fast-growing evidence base poses fundamental challenges to conventional (usually manual) synthesis methods.
Digital Evidence Synthesis Tools (DESTs) have been heralded to make the evidence synthesis process faster and cheaper without compromising quality.
However, the last 20 years of research and application have not lived up to this promise.
Advances have been marginal, limited to individual tasks, and there has been a lack of attention to researching their safe
and responsible use at scale.
Recent fundamental advances in artificial intelligence, particularly large-language models, promise a step-change across a broader range of complex
evidence synthesis tasks.
The next generation of DESTs require re-defined artificial-intelligence-powered human-machine interactions that can be applied safely and responsibly
without compromising methodological standards and trust.
Co-production is critical to effectively address the needs of decision makers in low- and high-income countries.

{{< figure src="fig1.png" id="impacts" >}}
