---
title: "Public Perception on Twitter"
summary: "Research on public perception of climate change, geoengineering, negative emission technologies, and more"
authors: ["Tim Repke", "Finn Müller-Hansen"]
tags: [data]
categories: []
date: 2023-04-14T17:39:13+02:00
lastmod: 2024-08-28T17:39:13+02:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

We use Twitter data for several projects on public perception.
Most importantly, as presented in our chapter on perceptions carbon dioxide removal in the [State of CDR](https://www.stateofcdr.org/), our goal is to develop methodologically sound methods to track and explain changes in the attention, sentiment, and emotions towards emerging climate technologies.
Doing so with the help of publicly available data allows us to perform the analyses in near-real-time and on historical data.
This approach complements studies in the social sciences that are based on focus groups and (large-scale) surveys and hence require longer update-intervals.

In order to process large amounts of data, we utilise two-dimensional semantic landscapes of embedded texts, state-of-the-art pre-trained classifiers, and embedding-based topic models.

So far, we have worked on, or have been involved in projects on
* Geoengineering on Twitter 
   * [Conspiracy spillovers and geoengineering](/publication/2023/conspiracy-spillovers-and-geoengineering) *(2023)*
* Greenhouse gas removal 
   * [Chapter 4 in  The State of Carbon Dioxide Removal](/publication/2023/state-of-carbon-dioxide-removal-1st-edition) *(2023)*
   * [Attention, sentiments and emotions towards emerging climate technologies on Twitter](/publication/2023/twitter-cdr-attention) *(2023)*
   * [Chapter 6 in  The State of Carbon Dioxide Removal](/publication/2024/state-of-carbon-dioxide-removal-2nd-edition) *(2024)*
   * [Growing online attention and positive sentiments towards carbon dioxide removal](/publication/2024/cdr-attention) *(2024, in revision)*
* Climate change discourse
   * [How global crises compete for our attention](publication/2024/twitter-vs-covid) *(2024)*
* Workshops 
   * We will participate in the *Using Social Media Data and Digital Platforms in Climate and Sustainability Research*, scheduled for 28-29 October 2024 at the International Institute for Applied Systems Analysis (IIASA) in Austria.

### Datasets & Source code

* [Data](https://zenodo.org/record/7778199) and [code](https://github.com/TimRepke/twitter-climate) for our work on how the onset of the pandemic temporarily shifted attention away from climate change
