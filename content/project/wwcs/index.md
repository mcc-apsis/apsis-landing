---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "What Works Climate Solutions Summit"
summary: "Synthesizing evidence for effective and equitable climate solutions"
authors: ["Jan Minx", "Diana Danilenko", "Niklas Döbbeling-Hildebrandt", "Andrea del Cura"]
tags: []
categories: []
date: 2024-06-09T17:39:13+02:00
#lastmod: 2024-08-28T17:39:13+02:00

# Optional external URL for project (replaces project detail page).
external_link: 

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false


links:
  - icon_pack: fas
    icon: link
    name: Website
    url: "https://whatworksclimate.solutions/"
  - icon_pack: fab
    icon: linkedin
    name: LinkedIn
    url: "https://www.linkedin.com/company/wwcs-summit/"

url_code:
url_pdf: 
url_slides:
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

In June 2024, Jan Minx and his team organised the inaugural What Works Climate Solutions Summit in Berlin.

The What Works Climate Solutions Summit is a high-level conference for evidence-based climate policy that took place for the first time on June 9–12, 2024. It aims to promote and catalyze synthetic evidence on climate solutions for upcoming climate change assessments – particularly the IPCC’s 7th Assessment Report ─ as well as other forms of scientific policy advice.
