---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Climate Literature Hub"
summary: "Database of our systematic maps and other evidence syntheses"
authors: ["Tim Repke"]
tags: [tools]
categories: []
date: 2024-02-03T17:39:13+02:00
lastmod: 2024-08-28T17:39:13+02:00

# Optional external URL for project (replaces project detail page).
external_link: 

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false


links:
  - icon_pack: fas
    icon: link
    name: Web
    url: "https://climateliterature.org"

url_code: "https://gitlab.pik-potsdam.de/mcc-apsis/living-evidence-maps/literature-hub"
url_pdf: 
url_slides:
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
Over at [climateliterature.org](climateliterature.org), we share all the datasets to all our systematic maps, reviews and other evidence synthesis projects.
We believe, that sharing is a highly important aspect of science.
We do these large mapping exercises so that other researchers or policymakers can hit the ground running by searching our curated database of high-quality and annotated datasets.

On the "Literature Hub", you can apply filters for most of the labels we coded (and download all of them).
At this point, the hub is still in a prototype stage.
However, we are planning to extend the functionality significantly.
Most importantly, the collections will be updated on a regular basis to provide "living evidence" that is always up to date.
You will be able to provide feedback (missing or erroneous data) or sign up for newsletters, so you get notified with new publications tailored to your interests. 
