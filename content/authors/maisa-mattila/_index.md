---
# Display name
title: Maisa Mattila

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
mcc_time: 2024–2024
user_groups:
  - Alumni
---

Hi :-)
