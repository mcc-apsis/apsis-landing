---
# Display name
title: Steve Kerr

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
mcc_time: 2023–2024
user_groups:
  - Alumni
---

Steve Kerr is a Master of Data Science for Public Policy student at the Hertie School and a Student Assistant at MCC where he supports the “Evidence for climate solutions” (ECS) group in researching the intersection of climate change and health. He previously worked at CPC Analytics where he contributed to data science projects for clients such as the World Health Organization. His research interests include global health, climate change, and the provision of public services in the digital age.

