---
# Display name
title: Tarun Khanna

# Is this the primary user of the site?
superuser: false

# Role/position
role: Visiting Researcher

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Visiting Scientists
---

Tarun M. Khanna is an Assistant Professor with the School of Public Policy and Global Affairs (SPPGA) and a visiting researcher at the Mercator Research Institute on Global Commons and Climate Change in Berlin. His research interests focus on energy and climate policy.

Dr. Khanna studied economics at the University of Delhi and specialized in energy economics during his PhD. He is interested in the economics of the decarbonization of the energy sector and the policies that are needed to get there. His wider research interests include evidence synthesis, policy evaluation, electricity markets and energy in development.

Before turning to academia, Dr. Khanna was a policy practitioner. He worked with regulators, governments, and utilities in the design and implementation of electricity policy in South Asia. Some of this work was done with the World Bank, the Asian Development Bank (ADB) and more recently the International Energy Agency (IEA). Over the last year, he contributed to the policy debate in Germany in face of the energy crisis in 2022 through reports for the German government on demand side-measures.
