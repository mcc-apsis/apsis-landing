---
# Display name
title: Finn Müller-Hansen

# Is this the primary user of the site?
superuser: false

# Role/position
role: Researcher


# Short bio (displayed in user profile at end of posts)
bio: Finn Müller-Hansen works in the group “Evidence for climate solutions” (ECS) on applications of machine learning and bibliometric analysis on the scientific literature and public as well as political discourse about energy transitions. He is interested in methods from complex systems theory to investigate the interplay of social, economic, and ecological factors in transformations towards a sustainable economy.

interests:
  - Machine learning and bibliometry applied to sustainability literature and science-policy discourse
  - Political economy of energy transitions and coal
  - Complex systems and network theory, social-ecological modeling


# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Researchers
  
---

Finn Müller-Hansen works in the group “Evidence for climate solutions” (ECS) on applications of machine learning and bibliometric analysis on the scientific literature and public as well as political discourse about energy transitions. He is interested in methods from complex systems theory to investigate the interplay of social, economic, and ecological factors in transformations towards a sustainable economy.

In his PhD at Potsdam Institute for Climate Impact Research and Humboldt University of Berlin, he  used network theory and agent-based modeling to study deforestation and land-use change in the Brazilian Amazon.
