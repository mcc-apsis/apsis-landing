---
# Display name
title: William Lamb

# Is this the primary user of the site?
superuser: false

# Role/position
role: Researcher

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Researchers
---

William Lamb is a research associate in the working group Applied Sustainability Science. He is interested in a human well-being framework for assessing climate change mitigation, with a focus on several areas of work:

* Country development pathways
* Human needs and energy consumption
* Carbon emissions budgets
* Political economy of climate change mitigation

William completed his PhD at the Tyndall Centre for Climate Change Research, University of Manchester in 2016. He obtained a Master’s degree in Ecological Economics from the University of Edinburgh in 2011.