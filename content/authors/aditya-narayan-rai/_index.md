---
# Display name
title: Aditya Narayan Rai

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# mcc_time: September 2024--?

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Research Assistants
---

Aditya Narayan Rai is a Student Researcher with the Applied Sustainability Science working group, currently enrolled in the MSc Data Science for Public Policy at Hertie School, Berlin.
