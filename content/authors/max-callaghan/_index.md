---
# Display name
title: Max Callaghan

# Is this the primary user of the site?
superuser: false

# Role/position
role: Researcher

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Researchers
---

Dr. Max Callaghan is a researcher in the Applied Sustainability Sciences working group. He works on quantitative methods to interpret large collections of scientific research (or “Big Literature”) about climate change with a view to 

* better understanding the science-policy interface, and
* assisting researchers and assessment makers in synthesising knowledge

He completed his Master’s degree in Public Policy at the Hertie School of Governance in 2016.