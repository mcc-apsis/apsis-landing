---
# Display name
title:  Padmavathi 'Padma' Bareddy

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# mcc_time: December 2024--?

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Research Assistants
---

My name is Padma Bareddy, currently pursuing a Master of Data Science for Public Policy at the Hertie School of Governance in Berlin.
I have a background in computer engineering, public policy, and hands-on experience in data-driven and action-oriented research within the public policy domain.
I have had an opportunity to travel and work across different states in India, including some of the extremely remote areas in central, eastern and northeast of India.
