---
# Display name
title: Lina Wallrafen

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
mcc_time: 2023–2024
user_groups:
  - Alumni
---

Lina works as a research assistant in the “Evidence for climate solutions” (ECS) group and currently assists in the screening for a research synthesis project for the Lancet Countdown on the impacts of climate change on health. 

She is currently pursuing a Master of Public Policy with focus on Policy Analysis at the Hertie School in Berlin and holds a B.Sc. in International Business & Economics from Maastricht University.


