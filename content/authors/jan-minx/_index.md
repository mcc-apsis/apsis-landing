---
# Display name
title: Jan Minx

# Is this the primary user of the site?
superuser: false

# Role/position
role: Head

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Principal Investigators
---

Dr. Jan Minx leitet die MCC-Arbeitsgruppe Angewandte Nachhaltigkeitsforschung. Er ist zudem Gastprofessor für Klimawandel und öffentliche Politik am Priestley International Centre for Climate an der Universität Leeds. Er arbeitet an einem breiten Spektrum von Themen in der Klima- und Nachhaltigkeitspolitik, wie z.B. Wege zur Klimaneutralität, die Rolle, das Potenzial, die Risiken und die Steuerung von Technologien zur CO2-Entnahme, Zielkonflikte und positive Nebeneffekte der Klimapolitik, Klimaschutz in Städten sowie sozialverträglicher globaler Kohleausstieg. Methodisch liegt ein Schwerpunkt seiner Arbeit auf der Evidenzsynthese: Einerseits geht es um den Einsatz künstlicher Intelligenz zur Skalierung von Methoden der Evidenzsynthese auf riesige Datenmengen und ihre Anwendung im Kontext globaler Umwelt-Assesements; andererseits geht es darum, neue Methoden der Evidenzsynthese zu entwickeln und so die wissenschaftliche Politikberatung und globale Umwelt-Assessments voranzubringen. Er arbeitet auch daran, wie sich mit Werkzeugen computergestützter Textanalyse traditionelle sozialwissenschaftliche Methoden auf riesige Gebilde skalieren lassen – um den Einfluss auf Klimadebatten und Framings zu messen.

Jan Minx hat wesentlich zu den jüngsten Arbeiten des Weltklimarats IPCC beigetragen. Er ist ein Koordinierender Leitautor des Sechsten IPCC-Sachstandsberichts, wo er in der Arbeitsgruppe III Minderung des Klimawandels das Kapitel zu Emissionstrends und -treibern mitverantwortet. Auch beim Fünften Sachstandsbericht spielte er eine wichtige Rolle, indem er als Leiter der technischen Unterstützungseinheit den Berichtsprozess koordinierte.

Jan Minx publiziert regelmäßig in einigen der angesehensten wissenschaftlichen Zeitschriften wie Science, Nature, Nature Climate Change oder den Proceedings of the National Academies of Sciences of the United States of America. Darüber hinaus hat er zu mehr als zwanzig Buchkapiteln und Berichten beigetragen. Laut Google Scholar wurde er mehr als 16.000 Mal zitiert, wobei die jährlichen Zitierungen im Jahr 2019 mehr als 3100 betragen. Sein h-Index beträgt 38 laut Google Scholar und 30 laut Web of Science. Vier seiner Publikationen wurden mehr als 1000-mal und zwanzig Publikationen mehr als 100-mal zitiert. Er ist Mitglied des Editorial Board von Environmental Research Letters und Carbon Balance and Management. Was die Medienwirksamkeit angeht, so hat Jan Minx Gastbeiträge und Blogartikel in deutschen und internationalen Zeitungen geschrieben, etwa in "The Guardian", "Huffington Post", "Washington Post", "Carbon Brief", "Die Zeit" oder "Wirtschaftswoche", und er ist regelmäßig im Radio präsent.