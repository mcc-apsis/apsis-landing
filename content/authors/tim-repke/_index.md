---
# Display name
title: Tim Repke

authors:
- Tim Repke

# Is this the primary user of the site?
superuser: false

# Role/position
role: Researcher

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Researchers
---

My main interest is making large heterogeneous corpora accessible for humans.
During my time at the Humboldt-University and Hasso Plattner Institute I worked on information extraction, text mining, and visualising text corpora in *semantic landscapes*.
In the “Evidence for climate solutions” (ECS), I get to apply my prior more theoretical work in practice to reveal salient patterns in millions of Tweets or thousands of research articles.

If you are interested to learn more or write your master's thesis with us, feel free to visit [my website](https://repke.eu) or contact me.
