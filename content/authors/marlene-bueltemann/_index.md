---
# Display name
title: Marlene Bültemann

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# mcc_time: November 2024--?

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Research Assistants
---

I am Marlene, a new student assistent in the ECS working group.
I am a third-semester master’s student in Information Systems (Wirtschaftsinformatik) at HU Berlin and hold two bachelor degrees in Business Administration and Business Computing as well as an apprenticeship as an Industrial Business Management Assistant (Industriekauffrau).

In my free time, I love to play handball and also coach a team of young girls.
