---
# Display name
title: Andres Mena

# Is this the primary user of the site?
superuser: false

# Role/position
role: Student assistant

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Research Assistants
---

Andrés is pursuing a Master in Integrated Natural Resource Management  at the Humboldt University in Berlin. He currently assists in the screening for a research synthesis project about the impacts of climate change on health.
