---
# Display name
title: Sarah Lück

# Is this the primary user of the site?
superuser: false

# Role/position
role: Researcher

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Researchers
---

Dr. Sarah Lück is a researcher in the working group Applied Sustainability Science. She is interested in analyzing large text corporas with the goal to create systematic reviews and living maps of research landscapes.

Sarah studied physics at the Potsdam University, completed her PhD on cellular biology at the Humboldt University and has worked as a Data Scientist for the news publisher Axel Springer.