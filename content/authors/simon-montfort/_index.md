---
# Display name
title: Simon Montfort

# Is this the primary user of the site?
superuser: false

# Role/position
role: Visiting Researcher

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

mcc_time: 2023

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Alumni
---

Hi :-)