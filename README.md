# APSIS Research Group website

This page is generated with [hugo](https://gohugo.io/) using the [Hugo Research Group Theme](https://github.com/wowchemy/starter-hugo-research-group).
For more details, on how to use this, check the [wowchemy documentation](https://wowchemy.com/docs/) or [hugo documentation](https://gohugo.io/documentation/) or the [blox documentation](https://bootstrap.hugoblox.com/blocks/collection/).

Note, that all wowchemy modules are merged into this repository to be independent of future (breaking) changes.
Furthermore, this simplifies making changes to partials and other parts of the theme.
The documentation might thus not be directly applicable.

All relevant content can be edited in the `content/` folder.
Please consult the respective documentations for details on the exact format, in some cases it might also be easier 
to look at the `archetypes/` for a particular type of content.

## Run locally
Just `apt install hugo`, navigate to this folder and execute `hugo server` to run a test server or `hugo` to build 
the html for upload or local hosting.

### Preparing vendor assets
We strive to serve the website completely without any external sources.
This is achieved by creating `static/js/vendor/main.min.js` as suggested in the [documentation](https://wowchemy.com/docs/hugo-tutorials/offline-site/) via
```bash
$ cat assets/js/_vendor/jquery.min.js assets/js/_vendor/medium-zoom.esm.js assets/js/_vendor/bootstrap.bundle.min.js assets/js/_vendor/headroom.min.js assets/js/_vendor/instantpage.js assets/js/_vendor/fuse.min.js assets/js/_vendor/jquery.mark.min.js assets/js/_vendor/leaflet.min.js > static/js/vendor/main.min.js
$ cat assets/css/_vendor/academicons.min.css assets/css/_vendor/leaflet.min.css > static/css/vendor/main.min.css 
```

You may need to remove `export default ...` lines here and there.

### Initial population of the website
The publications were automatically generated from the list on the MCC website.
`scripts/scrape_publications` contains a series of scripts that downloads the publications, 
enriches them with data from semantic scholar, and generates hugo publications from that.

It's not great (yet), but in case you do decide to update the script to properly populate the available meta-data, 
please make sure you don't destroy potential manual edits of a few publications.
